#include "Url.hpp"

#include <gtest/gtest.h>
#include <string>

#include <bson/bson.h>

#include "crawler/Logger.hpp"

using namespace std;

TEST( Url, ctor_onion )
{
  string o = "azeaze";
  Url u(o);
  ASSERT_EQ(u.getOnion(), o);
}

TEST( Url, from_bson_url )
{
  string o = "jsojsoisjoisjoijsoijs.onion";
  bson_t* document = BCON_NEW("url", BCON_UTF8 (o.c_str()));
  Url u(document);
  ASSERT_EQ(u.getOnion(), o);
}

TEST( Url, from_bson_title )
{
  string t = "titleTitle";
  bson_t* document = BCON_NEW("url", BCON_UTF8 ("url"),
			      BCON_UTF8 ("title"), BCON_UTF8 (t.c_str()));
  Url u(document);
  ASSERT_EQ(u.getTitle(), t);
}

TEST( Url, from_bson_deathnb )
{
  int death = 10;
  bson_t* document = BCON_NEW("url", BCON_UTF8 ("url"),
			      BCON_UTF8 ("death"), BCON_INT32 (death));
  Url u(document);
  ASSERT_EQ(u.getDeathNb(), death);
}

// Check with a & char in the URL
TEST( Url,  test_amp )
{
  string s("http://test.onion&amp;");
  Url u(s);
  
  string s2 = "torify httping -c 1 \"" + u.getOnion()  + "\""+ REDIRECT_FILE;
  int ret = system(s2.c_str());
  ASSERT_EQ(ret, 256);
}

// Must remove leading slash
TEST( Url, ctor_onion_remove_slash )
{
  string o = "azeaze/";
  Url u(o);
  ASSERT_EQ(u.getOnion(), "azeaze");
}

TEST( Url, from_bson_extractid )
{
  int death = 10;
  bson_oid_t id;
  bson_oid_init (&id, NULL);
  bson_t* document = BCON_NEW("_id", BCON_OID (&id));
  Url u(document);
  ASSERT_TRUE(bson_oid_equal(u.getId(), &id));
}

TEST( Url, has_anchor )
{
  ASSERT_EQ(Url::hasAnchor("url#aze"),     true);
  ASSERT_EQ(Url::hasAnchor("url#aze_iop"), true);
  ASSERT_EQ(Url::hasAnchor("url#aze-ert"), true);

  ASSERT_EQ(Url::hasAnchor("url.org"), false);
}

#define _ED(u) Url::extractDomain(u)
TEST( Url, extract_diomain)
{
  // Mainly from https://github.com/twitter/twitter-text/blob/master/conformance/extract.yml#L206
  ASSERT_EQ(_ED("http://example.com"), "http://example.com");
  ASSERT_EQ(_ED("https://example.com"), "https://example.com");
  /*  ASSERT_EQ(_ED("http://ああ.com"), "http://ああ.com");
  ASSERT_EQ(_ED("http://あ-あ.com"), "http://あ-あ.com");
  */
  ASSERT_EQ(_ED("https://exa-ple.com"), "https://exa-ple.com");

  ASSERT_EQ(_ED("http://aze.onion"), "http://aze.onion");

  // Subdomain
  ASSERT_EQ(_ED("http://aze.google.com"), "http://aze.google.com");

  // Complete URL
  ASSERT_EQ(_ED("http://aze.google.com/iouoiu"), "http://aze.google.com");
  ASSERT_EQ(_ED("http://google.com/iouoiu"), "http://google.com");
  ASSERT_EQ(_ED("http://google.com/iouoiu/aze"), "http://google.com");
  ASSERT_EQ(_ED("http://google.com#aze"), "http://google.com");
  ASSERT_EQ(_ED("http://google.com/#mlkmlk"), "http://google.com");
  ASSERT_EQ(_ED("http://google.com&321321"), "http://google.com");
  ASSERT_EQ(_ED("http://google.com&321321?32123"), "http://google.com");
}
