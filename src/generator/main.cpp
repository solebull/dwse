#include "common/Config.hpp"
#include "common/Database.hpp"

#include "Generator.hpp"

#include <ctime>
#include <unistd.h>

// Will generate c++ code to be compiled to webassembly
int
main(int argc, char** argv)
{
  srand( (unsigned) time(NULL) * getpid());
  Config c;
  Database d(&c);
  bool test  = argc > 1;
  Generator g(&c, &d, test);
  exit(0);
}
