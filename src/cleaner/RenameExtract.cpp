#include "RenameExtract.hpp"

#include <regex>
#include <string>
#include <iostream>

#include "common/Database.hpp"
#include "common/Url.hpp"

using namespace std;

ccRenameExtract::ccRenameExtract():
  CleanerCommand("mv-extract",
		 "extract url from a non-valid scheme-based string")
{
  
}

int
ccRenameExtract::run(Database* db)
{
  list<string> triggers =
    {
     "src=\'",
     "window.location.href",
     ";indexmenu",
     "action=",
     "\'"
    };
  
  int mod = 0; // Number of modified records
  auto urls = db->getAllUrls();
  
  for (auto& u : urls)
    {
      for (auto& t : triggers)
	{
	  if (u.find(t) != std::string::npos)
	    {
	      auto zurl = db->getUrl(u);
	      db->modifyUrl(zurl->getId(), remove(u));
	      ++mod;
	    }
	}
    }
  return mod;
}
  
string
ccRenameExtract::remove(const std::string& s)
{
  std::smatch m;
  std::regex e ("(https?://.*onion)");

  std::regex_search(s,m,e);
  return m[1];
}

