#include "common/Formatter.hpp"

#include <iostream>
#include <cstdlib>   // USES srand(), rand()

using namespace std;

/** A simple ncurses-based common formatter test
  *
  */
int
main()
{
  Formatter f;
  int running =0;
  int keycode;
  srand (time(NULL));

  int itemnb = 1;
  
  while (running==0)
    {
      int v=rand() % 4000;
      if (v==0)
	{
	  MainListItem mli;
	  mli.url="New item" + std::to_string(itemnb++);
	  f.add(&mli);
	}
      else if (v==1)
	{
	  f.addDomains(1);
	       
	}
      
      running=f.run(&keycode);
    }

  return running;
}
