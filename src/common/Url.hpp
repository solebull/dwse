#ifndef __URL_HPP__
#define __URL_HPP__

#include <string>
#include <ctime>
#include <ostream>

#include <bson/bson.h>

struct UrlFlags
{
  bool nsfw;
  bool nsfl;
};


/** Defines an url as found in the mongodb database
  *
  *
  *
  */
class Url
{
public:
  Url();
  Url(const std::string&);
  Url(const bson_t*);
  
  void randomize(void);

  void print(std::ostream&) const;
  
  friend std::ostream& operator<< (std::ostream& stream, const Url& u) {
    u.print(stream);
    return stream;
  }

  const std::string& toStr(void) const;

  // Getters
  const bson_oid_t*  getId(void) const;
  const std::string& getOnion(void) const;
  const std::string& getTitle(void) const;
  int                getDeathNb(void) const;
  int                getLiveBackAfter(void) const;
  time_t             getCrawledTime(void) const;

  // Statics
  static bool        hasAnchor    (const std::string&);
  static std::string extractDomain(const std::string&);
  
protected:
  std::string random_string( size_t length );
  
private:
  std::string doc;    //!< A string representation of the BSON document
  bson_oid_t  id;     //!< The mongodb document id
  std::string onion;  //!< The onion URL
  std::string title;  //!< The webpage title
  int deathnb;        //!< Number of non-crawlable encounter
  int liveBackAfter;  //!< How many death before being live back ?
  
  time_t lastCrawl;   //!< Last time we visit this URL, from 1970, in seconds
  
  UrlFlags autoFlags;   //!< Automatically-set safe-flags
  UrlFlags manualFlags; //!< Manually reviewed flags
};

#endif // !__URL_HPP__
