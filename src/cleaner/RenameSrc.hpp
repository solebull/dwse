#ifndef __REMOVE_SRC_HPP__
#define __REMOVE_SRC_HPP__

#include "CleanerCommand.hpp"

#include <string>

/** Rename invalid (uncrawlable) URL nested in href('%s') style urls
  *
  */
class ccRenameSrc : public CleanerCommand
{
public:
  ccRenameSrc();
  virtual int run(Database* db);
  
protected:
  std::string remove(const std::string&);
};

#endif // !__REMOVE_SRC_HPP__

