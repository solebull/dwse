#include "RemoveEmptyUrls.hpp"

#include "common/Database.hpp"
#include "common/Url.hpp"

#include <iostream>

using namespace std;

ccRemoveEmptyUrls::ccRemoveEmptyUrls():
  CleanerCommand("rm-empty", "Remove empty urls from the database")
{
  
}

int
ccRemoveEmptyUrls::run(Database* db)
{
  int mod = 0;
  
  // Count empty urls
  auto urls = db->getAllUrls();
  int empty = 0;
  for (auto& u : urls)
    if (u.empty())
      ++empty;

  // Remove ony by one
  for (int i=0; i < empty; ++i)
    {
      auto u = db->getUrl("");
      db->deleteById(u->getId());
      ++mod;
    }
  
  return mod;
}
