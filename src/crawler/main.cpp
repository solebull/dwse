#include <mongoc/mongoc.h>

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include "common/Config.hpp"
#include "common/Database.hpp"

#include "Crawler.hpp"

#include "Logger.hpp"

int
main (int argc, char *argv[])
{
  L("Starting crawler");
  srand (time(NULL));
  
  Config c;
  Database d(&c);
  Crawler cr(&c, &d);

  for (auto page : c.getStartPages())
    {
      d.upsertUrl(page);
      //      cr.crawl(page);
    }

  d.getRandomUrl();
  
  while (true)
    cr.crawl(d.getRandomUrl());

  return 0;
}
