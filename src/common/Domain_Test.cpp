#include "Domain.hpp"

#include <gtest/gtest.h>

using namespace std;

TEST( Domain, ctor_onion )
{
  string o = "azeaze";
  Domain u(o);
  ASSERT_EQ(u.getDomain(), o);
}
