const { MongoClient } = require("mongodb");
var uri = "mongodb://localhost:27017";

/** The dabatase class
  *
  * Note: function prefixed with 'p_' return a promise obehct. You must
  * call .then to get value :
  *	    var p = db.urlsCount();
  *	    p.then(function(v){
  *		consol.log("V=" + v);
  *		assert.equal(v, 0);
  *	    });
  *
  */
class Database {
    constructor(test) {
	this.client = new MongoClient(uri, {
	    useNewUrlParser: true,
	    useUnifiedTopology: true,
	});

	this.client.connect();
	this.dbname = 'dwse';
	if (test == true)
	    this.dbname = 'dwse-test';
	
	this.db = this.client.db(this.dbname);
    }

    get name() {
	return this.db.databaseName;
    }
    
    get urls() {
	return this.db.collection("urls");
    }
    
    p_urlsCount() {
	const urls = this.db.collection("urls");
	return urls.countDocuments({});
    }

    p_domainsCount() {
	const urls = this.db.collection("domains");
	return urls.countDocuments({});
    }

    p_query(query, options, sort) {
	const urls = this.db.collection("urls");
	return urls.find(query, options).sort(sort).toArray();
    }

    /** Upsert a document in urls collection */
    upsertUrl(vurl, document) {
	const urls = this.db.collection("urls");
	urls.updateOne({url: vurl}, { $set: document }, {upsert: true})
            .catch((err) => {
                console.log('Error: ' + err);
            });
    }

    deleteUrl(vurl) {
	const urls = this.db.collection("urls");
	urls.deleteOne({url: vurl})
            .catch((err) => {
                console.log('Error: ' + err);
            });
    }

};

/*
async function run() {
  try {
    await client.connect();
    const database = client.db("dwse-test");
    const urls = database.collection("urls");
    // Query for a movie that has the title 'The Room'
    const query = {  };
    const options = { };
    const url = await urls.findOne(query, options);
    // since this method returns the matched document, not a cursor, print it directly
    console.log(url);
  } finally {
    await client.close();
  }
}
run().catch(console.dir);
*/
module.exports = Database;
