#include "Domain.hpp"

using namespace std;

Domain::Domain(const std::string& d):
  domain(d)
{
  
}

Domain::Domain(const bson_t* bs)
{
  char* str;
  str = bson_as_canonical_extended_json (bs, NULL);
  doc = str;
}


const std::string&
Domain::getDomain(void) const
{
  return domain;
}

const std::string& Domain::toStr(void) const
{
  return doc;
}
