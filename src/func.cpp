#include <gtest/gtest.h>

// Home of the functionnal tests
int main(int ac, char* av[])
{
  testing::InitGoogleTest(&ac, av);
  return RUN_ALL_TESTS();
}
