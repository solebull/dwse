#include "RemoveBlobs.hpp"

#include "common/Database.hpp"
#include "common/Url.hpp"

#include <iostream>

using namespace std;

ccRemoveBlobs::ccRemoveBlobs():
  CleanerCommand("rm-blobs", "Remove blobs based of end of urls")
{

  extlist = { ".jpg", ".JPG", ".jpeg", ".JPEG", ".png", ".gif" };
  
}
  
int
ccRemoveBlobs::run(Database* db)
{
  int mod = 0;
  
  // Count empty urls
  auto urls = db->getAllUrls();
  for (auto& u : urls)
    for (auto& e : extlist)
      if (isExtension(u, e))
	{
	  db->deleteUrl(u);
	  ++mod;
	}
  cout << mod << " removed urls" << endl;
  return mod;
}

bool
ccRemoveBlobs::isExtension(const string& fullString, const string&ending) const
{
  // From https://stackoverflow.com/a/874160
  if (fullString.length() >= ending.length())
    {
      return (0 == fullString.compare (fullString.length() - ending.length(),
				       ending.length(), ending));
    }
  else
    {
      return false;
    }
}
