const isQueryEmpty = require("./query");

var assert = require('assert');
describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});


describe('Query', function() {
  describe('#isQueryEmpty', function() {
    it('should return true for empty/undefined query', function() {
	assert.equal(isQueryEmpty(), true);
	assert.equal(isQueryEmpty(undefined), true);
	assert.equal(isQueryEmpty(''), true);
	assert.equal(isQueryEmpty('   '), true);
    });

    it('should return false for a text query', function() {
	assert.equal(isQueryEmpty('aze'), false);
    });
  });
});
