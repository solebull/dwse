#include "Formatter.hpp"

#include <gtest/gtest.h>

using namespace std;

class TestableFormatter : public Formatter
{
public:
  TestableFormatter(): Formatter(false){ }

  std::string _elaToStr(double d){ return elaToStr(d); }
  std::string _intToStr(unsigned int i){ return intToStr(i); }
};

std::string s(const char* c) {
  return string(c);
}

/// 3 digist after the coma
TEST( Formatter, elaStr_3floats )
{
  TestableFormatter f;
  ASSERT_EQ(s("000:00:06.123"), f._elaToStr(6.123));
  ASSERT_EQ(s("000:00:06.123"), f._elaToStr(6.12322));
  ASSERT_EQ(s("000:00:06.123"), f._elaToStr(6.12356));

  ASSERT_EQ(s("000:00:12.122"), f._elaToStr(12.123));
}

TEST( Formatter, elaStr_minutes )
{
  TestableFormatter f;
  ASSERT_EQ(s("000:01:02.121"), f._elaToStr(62.122));
  ASSERT_EQ(s("000:01:46.102"), f._elaToStr(106.102));
}

TEST( Formatter, intToStr )
{
  TestableFormatter f;
  ASSERT_EQ(s("123"), f._intToStr(123));
  ASSERT_EQ(s("123.46K"), f._intToStr(123456));
  ASSERT_EQ(s("123.46M"), f._intToStr(123456789));
}

