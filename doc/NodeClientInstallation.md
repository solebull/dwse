# Node client Installation

The node client is located in `stc/nodeclient`. Since v0.0.2-8, it's the
only official/running client.

Ro test the current installation, you can use `run_client.sh` from the
root directory.

## Installation

The way we install here is based both in `pm2` to start the process and
`nginx` as reverse proxy :

	cd src/nodeclient/
	npm install
	sudo npm install pm2@latest -g
	PORT=3007 pm2 start ./bin/www --name dwse
	pm2 startup systemd
	
and follow instructions. then :
	
	pm2 save

Now, to get informations :

	systemctl status pm2-{username}}

Then, in your ngix configuration file (`/etc/nginx/sites-available/default`) :

	location /dwse {
        proxy_pass http://localhost:3007;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
