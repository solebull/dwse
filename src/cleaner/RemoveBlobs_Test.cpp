#include "RemoveBlobs.hpp"

#include <gtest/gtest.h>

using namespace std;

class ccRemoveBlobsTest : public ccRemoveBlobs
{
public:
  bool _isExtension(const string& url, const string& ext) const{
    return isExtension(url, ext);
  }
};

TEST( ccRemoveBlobs, dot_jpg )
{
  string file = "testtesttest.jpg";
  ccRemoveBlobsTest r;
  
  ASSERT_TRUE(r._isExtension(file, ".jpg"));
  ASSERT_FALSE(r._isExtension(file, ".gif"));
  ASSERT_FALSE(r._isExtension(file, "test."));
}
