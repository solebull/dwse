#ifndef __GENERATOR_HPP__
#define __GENERATOR_HPP__

// Forward declarations
class Config;
class Database;
// End of forward declarations

class Generator
{
public:
  Generator(Config*, Database*, bool test=false);
};


#endif // ! __GENERATOR_HPP__
