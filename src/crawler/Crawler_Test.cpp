#include "Crawler.hpp"

#include <gtest/gtest.h>
#include <string>

#include <bson/bson.h>

using namespace std;

class CrawlerTest : public Crawler
{
public:
  CrawlerTest():Crawler(){}
  string _extractTitle(const string& o){ return extractTitle(o); };
  bool   _isAlive(const string& o){ return isAlive(o); };
  string _escapeUrl(const string& o){ return escapeUrl(o); };
  string _removeMarkup(const string& o){ return removeMarkup(o); };
  bool _keepWord(const string& o){ return keepWord(o); };
  string _truncateUrl(const string& o, int w){ return truncateUrl(o, w); };
};

TEST( Crawler,  extract_title )
{
  string o = "<html><title>aze</title></html>";
  CrawlerTest c;
  string t = c._extractTitle(o);
  ASSERT_EQ(t, "aze");
}

TEST( Crawler,  extract_title_no_title )
{
  string o = "<html><h1>aze</h1></html>";
  CrawlerTest c;
  string t = c._extractTitle(o);
  ASSERT_EQ(t, "");
}

TEST( Crawler,  extract_title_upcase )
{
  string o = "<html><TITLE>aze</Title></html>";
  CrawlerTest c;
  string t = c._extractTitle(o);
  ASSERT_EQ(t, "aze");
}

TEST( Crawler,  extract_title_keepcase )
{
  string o = "<html><title>Aze</title></html>";
  CrawlerTest c;
  string t = c._extractTitle(o);
  ASSERT_NE(t, "aze");
  ASSERT_EQ(t, "Aze");
}

// Check that title is trimmed from whitespaces
TEST( Crawler,  escape_url )
{
  string o = "cmd";
  CrawlerTest c;
  string t = c._escapeUrl(o);
  ASSERT_EQ(t, "\"cmd\"");
}

// Remove slash based markup
TEST( Crawler,  remove_markup_slash )
{
  string o = "</span>aze";
  CrawlerTest c;
  string t = c._removeMarkup(o);
  ASSERT_EQ(t, "aze");
}


// Remove most of the HTML markups
TEST( Crawler,  remove_markup )
{
  string o = "<html>aze</html>";
  CrawlerTest c;
  string t = c._removeMarkup(o);
  ASSERT_EQ(t, "aze");
}

// Remove comma and others punctuation
TEST( Crawler,  remove_punc )
{
  string o = "<footer>aze,</footer>";
  CrawlerTest c;
  string t = c._removeMarkup(o);
  ASSERT_EQ(t, "aze");
}

// Remove comma and others punctuation
TEST( Crawler,  keep_word )
{
  CrawlerTest c;
  ASSERT_EQ(false, c._keepWord("no"));  // Too short
  ASSERT_EQ(false, c._keepWord("ask"));  // Too short
}

// Remove comma and others punctuation
TEST( Crawler,  truncateUrl )
{
  CrawlerTest c;
  ASSERT_EQ(c._truncateUrl("azeazeaze", 6), "aze...");
}

