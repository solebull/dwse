#ifndef __REMOVE_HREF_HPP__
#define __REMOVE_HREF_HPP__

#include "CleanerCommand.hpp"

#include <string>

/** Rename invalid (uncrawlable) URL nested in href('%s') style urls
  *
  */
class ccRenameHref : public CleanerCommand
{
public:
  ccRenameHref();
  virtual int run(Database* db);
  
protected:
  std::string remove(const std::string&);
};

#endif // !__REMOVE_HREF_HPP__

