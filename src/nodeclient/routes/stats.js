var express = require('express');
var router = express.Router();

const Database = require("../db");

async function run() {
  try {
      const db = new Database();
      const urls = db.urls;
      const nbindexed = await urls.countDocuments({});
      const nbdomains = await db.p_domainsCount({});
      console.log("nbdomains++" + nbdomains);
      const stats = await urls.stats({});
      var sts = stats.storageSize;
      if (sts > 1000000)
	  sts = "" + stats.storageSize / 1000000 + " Mo";
          
      router.get('/', function(req, res, next) {
	  //  res.send('respond with a resource');
	  res.render('stats', { title: 'dwse',
				nbindexed: nbindexed,
				nbdomains: nbdomains,
				storageSize: sts});
      });
  } finally {
//    await client.close(); // Close between each query
  }
}
run().catch(console.dir);
module.exports = router;
