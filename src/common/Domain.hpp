#ifndef __DOMAIN_HPP__
#define __DOMAIN_HPP__

#include <string>
#include <bson/bson.h>


/** Represent an onion domain/subdomain in the database
  * 
  * Is extraction occurs from Url::extractDomain static function.
  *
  * It'll be responsible of its own extract from the database's bson document.
  *
  */
class Domain
{
public:
  Domain(const std::string&);
  Domain(const bson_t*);

  const std::string& getDomain(void) const;

  const std::string& toStr(void) const;

private:
  std::string domain; //!< The string known as the domain name (.onion)

  std::string doc; //!< The full mongodb document
};

#endif // !__DOMAIN_HPP__

