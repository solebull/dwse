#include "Generator.hpp"

#include "common/Config.hpp"
#include "common/Database.hpp"
#include "common/Url.hpp"

#include "Document.hpp"

#include <iostream>

using namespace std;

/** \param test If true, will generate test (i.e. false) results */
Generator::Generator(Config* c, Database* db, bool test)
{

  if (test)
    cout << "Test content mode enabled..." << endl;

  
  const char* filename = "gen-out.cpp";
  Document d(db, test);
  ofstream x(filename);
  x << d;
  x.close();

  const char* jsfile = "gen-out.js";
  ofstream x2(jsfile);
  d.to_js(x2);
  x2.close();

  /*
  if (test)
    {
      cout << "Test content mode enabled..." << endl;
      for (int i=0; i<100; ++i)
	{
	  Url u;
	  u.randomize();
	  cerr << u;
	}
    }
  else
    {
      d->getAllUrls();
    }
  */
  cout << "Generator saved output in '" << filename << "'" << endl;
  cout << "Now call `em++ " << filename
       << " -o ../src/www/generator-result.wasm "
       << "-O1 -s WASM=1 -s SIDE_MODULE=1`." << endl;

  cout << "JS file " << jsfile << " (cp " << jsfile << " ../src/www/)" << endl;
  
}
