#ifndef __REMOVE_EMPTY_URLS_HPP__
#define __REMOVE_EMPTY_URLS_HPP__

#include "CleanerCommand.hpp"

// Forward declaration
class Database;
// End of forward declaration


/** Remove empty urls from the database
  * 
  */
class ccRemoveEmptyUrls : public CleanerCommand
{
public:
  ccRemoveEmptyUrls();
  virtual int run(Database* db);
};


#endif // !__REMOVE_EMPTY_URLS_HPP__
