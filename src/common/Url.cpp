#include "Url.hpp"

#include <iostream>

#include <ctime>
#include <unistd.h>
#include <regex>

using namespace std;

Url::Url():
  title(""),
  deathnb(0),
  lastCrawl(0),
  liveBackAfter()
{

}

Url::Url(const string& o):
  Url()
{
  this->onion = o;

  // Remove leading slash to avoid double entries
  if (onion.back() == '/')
    onion.pop_back();
}

/** Construct a new Url object from a bson document
  *
  * Will try to extract values from the given document.
  *
  */
Url::Url(const bson_t* doc):
  Url()
{

  // Convert to string
  if (doc)
    {
      char* str = bson_as_canonical_extended_json (doc, NULL);
      if (str)
	this->doc = string(str);
    }
  
  // Extract URL field value
  bson_iter_t iter;
  bson_iter_t child;

  //  if (bson_iter_init_find (&iter, doc, "url"))
    {
      if (bson_iter_init (&iter, doc)) {
	while (bson_iter_next (&iter)) {

	  const bson_value_t * value = bson_iter_value (&iter);
	  string key((char*)bson_iter_key (&iter));
	  if (value)
	    {
	      
	      if (key == "_id")
		{
		  bson_oid_t val = value->value.v_oid;
		  this->id = val;
		}
	      else if (key == "url")
		{
		  string val((char*)value->value.v_utf8.str);
		  this->onion = val;
		}
	      else if (key == "title")
		{
		  string val((char*)value->value.v_utf8.str);
		  this->title = val;
		}
	      else if (key == "death")
		{
		  int val = value->value.v_int32;
		  this->deathnb = val;
		}
	      else if (key == "crawled-date")
		{
		  auto val = value->value.v_datetime;
		  //		  cout << "Extracted date time is " << val << endl;
		  this->lastCrawl = val / 1000; // in seconds, not ms
		}
	      else if (key == "live-back-after")
		{
		  int val = value->value.v_int32;
		  this->liveBackAfter = val;
		}
	    }
	}
      }
    }
}


void
Url::randomize(void)
{
  onion=random_string(10) + ".onion";
}

void
Url::print(std::ostream& s) const
{
  s << onion;
}

std::string
Url::random_string( size_t length )
{
  static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
  std::string str;
  str.reserve(length);
  //  std::generate_n( str.begin(), length, randchar );
  for (int i = 0; i < length; ++i) 
        str += alphanum[rand() % (sizeof(alphanum) - 1)];
  
  return str;
}

const std::string&
Url::getOnion(void) const
{
  return this->onion;
}

const std::string&
Url::getTitle(void) const
{
  return title;
}

int
Url::getDeathNb(void) const
{
  return deathnb;
}

time_t
Url::getCrawledTime(void) const
{
  return lastCrawl;
}

int
Url::getLiveBackAfter(void) const
{
  return liveBackAfter;
}

const bson_oid_t*
Url::getId(void) const
{
  return &id;
}

/** Static function to be used before we parsed the full onion */
bool
Url::hasAnchor(const std::string& url)
{
  return url.find("#") != std::string::npos;
}

string
Url::extractDomain(const std::string& url)
{
  std::smatch m;
  std::regex e ("(https?://[a-zA-Z2-7-\\.]*\\.[a-zA-Z0-9]*)");

  std::regex_search(url,m,e);
  return m[1];
}

const std::string&
Url::toStr(void) const
{
  return doc;
}
