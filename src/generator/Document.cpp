#include "Document.hpp"

#include "common/Database.hpp"
#include "common/Url.hpp"

#include <iostream>
//#include <bits/stdc++.h>

using namespace std;

Document::Document(Database* db, bool test)
{
  if (test)
    {
      startPages.push_back("startpage1.onion");
      startPages.push_back("startpage2.onion");
      
      for (int i=0; i<100; ++i)
	{
	  Url u;
	  u.randomize();
	  //	  urls.push_back(u);
	}
    }
  else
    {
      auto ur = db->getAllUrls();
      for (auto u : ur)
	{
	  // If already added => early exit
	  if (std::find(addedUrls.begin(), addedUrls.end(), u)
	      != addedUrls.end())
	    {
	      cout << ".";
	      cout.flush();
	      continue;
	    }
	  
	  // Prepare word list
	  auto _u = db->getUrl(u);
	  auto ws = db->getWords(_u->getId());
	  //	  	  cout << "Getting words for " << u << endl;

	  for (auto& w : ws)
	    words.insert(w.w);

	  addedUrls.push_back(u);
	  
	  if (ws.size() > 0)
	    {
	      cout << ws.size() << "+";
	      cout.flush();
	    }
	}

      cout << endl;
      
    }
}

void
Document::print(std::ostream& s) const 
{
  // Header includes
  s << "#include <string>" << endl;
  s << "#include <list>" << endl;
  s << "#include <emscripten/emscripten.h>" << endl;
  
  s << "using namespace std;" << endl;
  
  // Definition
  s << "struct Url{" << endl;
  s << "  string onion;" << endl;
  s << "  Url(string o):onion(o){}" << endl;
  s << "};" << endl;

  s << "list<string> startPages;" << endl;
  s << "list<Url> urls;" << endl;


  s << "void EMSCRIPTEN_KEEPALIVE init() {" << endl;
  // Content
  for (auto& sp: startPages)
    {
      s << "  startPages.push_back(\"" << sp << "\");" << endl;
    }
  
  /*  for (auto& u: urls) // SHOULD BE WORDS
    {
      s << "  urls.emplace_back(Url(\"" << u;
      s << "\"));" << endl;
    }
  */
  s << "}";
}

void
Document::to_js(std::ostream& s)
{
  s << "function getStartPages(){"
    << "  var arr=[];";

    for (auto& sp: startPages)
      {
      s << "  arr.push('" << sp << "');" << endl;
    }
  s << "  return arr;";
  s << "}";

  s << "function getWords(){"
    << "  var arr=[];";

  cout << words.size() << " words" << endl;
  if (words.empty())
    cout << "WARNING: Generating an empty words array." << endl;
  
  for (auto& sp : words)
    {
      s  << "  arr.push('" << sp << "');" << endl;
    }
  s << "return arr;}";

}
