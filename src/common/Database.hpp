#ifndef __DATABASE_HPP__
#define __DATABASE_HPP__

#include <vector>
#include <string>
#include <list>
#include <set>
#include <libmongoc-1.0/mongoc.h>

// Forward declaration
class Config;
class Domain;
class Url;
// End of forward declaration

struct Word{
  std::string w;     //!< The word
  int         n;     //!< The number of occurence
};

/** A flag container type */
typedef std::set<std::string> flags;

// Mongodb-powered document database
class Database
{
public:
  Database(Config*, bool test=false);
  ~Database();

  std::vector<std::string> getAllUrls();
  
  void        upsertUrl(const std::string&);
  void        modifyUrl(const bson_oid_t*, const std::string&);
  void        deleteUrl(const std::string&);
  void        deleteById(const bson_oid_t*);

  time_t      setCrawlTime(const std::string&);
  void        setTitle(const std::string&, const std::string&);
  void        setBackOnline(const std::string&);

  void        addWords(const std::string&, const std::list<std::string>&);
  std::list<Word>  getWords(const bson_oid_t*);

  // Blobs
  int         getBlobsNumber(void);
  void        deleteBlob(const std::string&);
  void        upsertBlob(const std::string&, const std::string&);

  // Domains
  int         getDomainsNumber(void);
  void        deleteDomain(const std::string&);
  void        upsertDomain(const std::string&);
  Domain*     getDomain(const std::string&);
  
  // Flags: we use an Url* here to avoid duplicates with Domain* functions
  void        setFlags(Url*, const flags&);
  flags       getFlags(Url*);
  void        setFlags(Domain*, const flags&);
  flags       getFlags(Domain*);
  
  std::string getRandomUrl();

  void incDeath(const std::string&);

  Url* getUrl(const std::string&);

protected:
  std::string toAscii(const std::string& s)const;
  std::string toBsonStr(const std::string& s)const;
  
private:
  mongoc_client_t*     client;

  // Collections
  mongoc_collection_t* col_urls;  //!< The URLs collection
  mongoc_collection_t* col_blbs;  //!< The Blobs collection
  mongoc_collection_t* col_doms;  //!< The Blobs collection

  int                  urlnb;     //!< Keep track of url numbers
  bool                 test;
};

#endif // !__DATABASE_HPP__
