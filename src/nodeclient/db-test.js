const Database = require("./db");
var assert = require('assert');

describe('Database', function() {
    describe('#getName()', function() {
	it('should return test db if true passed', function() {
	    const db1 = new Database(false);
	    assert.equal(db1.name, 'dwse');

	    const db2 = new Database(true);
	    assert.equal(db2.name, 'dwse-test');
	});
    });
    describe('#getDocumentsCount()', function() {
	it('should return an integer, not a promise', async () => {
	    const db = new Database(true);
	    var c = await db.p_urlsCount();
	    assert.equal(typeof(c), 'number');
	});
	it('should be 0', async () => {
	    const db = new Database(true);
	    let count = await db.p_urlsCount();
	    assert.equal(count, 0);
	});
    });
    describe('#getDomainsCount()', function() {
	it('should return an integer, not a promise', async () => {
	    const db = new Database(true);
	    var p = await db.p_domainsCount();
	    assert.equal(typeof(p), 'number');
	});
    });
    describe('#urls', function() {
	it('should return an object', function() {
	    const db = new Database(true);
	    const urls = db.urls;
	    assert.equal(typeof(urls), "object");
	});
	it('should have an upsertUrl function',  async () => {
	    const url = "aze";
	    const db = new Database(true);
	    var u1 = await db.p_urlsCount();
	    db.upsertUrl(url, {url: "aze"});
	    var u2 = await db.p_urlsCount();
//	    assert.equal(u1 + 1, u2);
	    db.deleteUrl(url);
	});
    });
    describe('#query()', function() {
	it('should return an array', function() {
	    const db = new Database(true);
	    const p = db.p_query({}, {});
	    p.then(function(v){
		assert.equal(v.length, 0);
		assert.equal(typeof(v), 'array');
	    });
	});
	/** Note: this test is not based on current implementation (used by
 	  * the client's index route) but on the sort option parameter.
	  *
	  */
	it('should return an array sorted by word occurence', async () => {
	    const db = new Database(true);
	    db.upsertUrl("aze", {
		words: {
		    word1: 1
		}});
	    db.upsertUrl("aze2", {
		words: {
		    word1: 3
		}});
	    db.upsertUrl("aze3", {
		words: {
		    word1: 2
		}});
	    const arr = await db.p_query({}, {}, { "words.word1": -1});
	    assert.equal(arr.length, 3);
	    console.log(JSON.stringify(arr));
	    console.log(JSON.stringify(arr[0]));
	    console.log("W0+"+arr[0].words.word1);
	    assert.equal(arr[0].words.word1, 3);
	    assert.equal(arr[1].words.word1, 2);
	    assert.equal(arr[2].words.word1, 1);

	    db.deleteUrl("aze");
	    db.deleteUrl("aze2");
	    db.deleteUrl("aze3");
	});
    });
});
