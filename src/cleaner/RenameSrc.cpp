#include "RenameSrc.hpp"

#include "RenameHref.hpp"

#include "common/Database.hpp"
#include "common/Url.hpp"

#include <algorithm> // USES std::find()
#include <iostream>

using namespace std;

ccRenameSrc::ccRenameSrc():
  CleanerCommand("rm-src", "Rename invalid src-based urls")
{

}

int
ccRenameSrc::run(Database* db)
{
  int mod = 0; // Number of modified records
  auto urls = db->getAllUrls();

  for (auto& u : urls)
    {
      if (u.find("src='") != std::string::npos &&
	  u.back() == '\'')
	{
	  auto zurl = db->getUrl(u);
	  db->modifyUrl(zurl->getId(), remove(u));
	  
	  mod++;
	}
    }
  
  

    return mod;
}

std::string
ccRenameSrc::remove(const std::string& s)
{
  if (s.find("src='") != std::string::npos && s.back() == '\'')
    {
      return s.substr(5, s.size() - 6);
    }

  return s;
}
