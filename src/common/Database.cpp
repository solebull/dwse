#include "Database.hpp"

#include <string>
#include <iostream>
#include <time.h>
#include <clocale> // USES setlocale()
#include <cctype>  // USES isalnum()

#include <boost/algorithm/string/trim.hpp>

#include "Config.hpp"
#include "Domain.hpp"
#include "Url.hpp"
using namespace std;

#define TCOUT(args) if (!test){ cout << args << endl; }

/** Database constructor
  *
  * Note: this constructor sets locale.
  *
  * \param c    A Config object pointer
  * \param test Are we in functionnal tests ?
  *
  */
Database::Database(Config* c, bool test):
  test(test),
  col_urls(NULL),
  col_blbs(NULL),
  col_doms(NULL),
  urlnb(0)
{
  std::setlocale(LC_ALL, "en_US.utf8");
  
  // Set dbname according to test value
  string dbname = c->getDbName();
  if (test)
    dbname = c->getDbName() + "-test";


  TCOUT("Opening database '" << dbname << "' ...");
  bson_error_t error;
  mongoc_uri_t *uri;
  
  mongoc_init ();
  //  client = mongoc_client_new(c->getMongoUri().c_str());

  /*
   * Safely create a MongoDB URI object from the given string
   */
   uri = mongoc_uri_new_with_error (c->getMongoUri().c_str(), &error);
   if (!uri) {
     fprintf (stderr,
	      "failed to parse URI: %s\n"
	      "error message:       %s\n",
	      c->getMongoUri().c_str(),
	      error.message);
     throw runtime_error(string("Can't get URI: ") + error.message);
   }
   else
     {
       TCOUT(" - Got mongodb URI");
     }
   /*
    * Create a new client instance
    */
   client = mongoc_client_new_from_uri (uri);
   if (!client) {
     throw runtime_error("Cannot get mongo client");
   }
   else
     {
       TCOUT(" - Got mongoc client");
     }

   
  // The urls collction
   col_urls = mongoc_client_get_collection (client, dbname.c_str(), "urls");
   col_blbs = mongoc_client_get_collection (client, dbname.c_str(), "blobs");
   col_doms = mongoc_client_get_collection (client, dbname.c_str(), "domains");
   TCOUT(" - Got mongoc collections (" << c->getDbName() << ".*)");


  // Adding an example URL
  time_t timestamp = time( NULL );
  struct tm* now = localtime( & timestamp );
  /*  crawled.tm_year = 6;
  crawled.tm_mon = 11;
  crawled.tm_mday = 9;
  */

  bson_t   *document;
  /*
  document = BCON_NEW
    (
     "crawled", BCON_DATE_TIME (mktime (now) * 1000),
     "url", BCON_UTF8 ("jsojsoisjoisjoijsoijs.onion"),
     "flags", "[",
       "{", "nsfw", "false", "}",
       "{", "nsfl", "false", "}",
     "]",
     );
  */

  /*
  const char *json = "{\"name\": {\"first\":\"Grace\", \"last\":\"Hopper\"}}";
  document = bson_new_from_json ((const uint8_t *)json, -1, &error);
  if (!mongoc_collection_insert_one(col_urls, document, NULL, NULL, &error))
    {
      fprintf (stderr, "Error inserting document: %s\n", error.message);
    }
  */
}

Database::~Database()
{
  mongoc_client_destroy (client);
  mongoc_cleanup ();
}

/** Get all URLs found in database
  *
  *
  */
std::vector<std::string>
Database::getAllUrls()
{
  mongoc_cursor_t *cursor;
  char *str;
  const bson_t *doc;
  bson_t *query;
  //  bson_t *doc;
  int64_t count;
  bson_error_t error;

  std::vector<std::string> ul;
  
  // count
  count = mongoc_collection_estimated_document_count(col_urls, NULL,
						     NULL, NULL, &error);
  if (count < 0) {
    fprintf (stderr, "Can't get document count : %s\n", error.message);
  }
  
  //  bson_t* opts = BCON_NEW ("url", BCON_INT32(1), "_id", BCON_INT32(0));
  query = bson_new ();
  cursor = mongoc_collection_find_with_opts (col_urls, query, NULL, NULL);
  int c= 0;
  while (mongoc_cursor_next (cursor, &doc)) {
    str = bson_as_canonical_extended_json (doc, NULL);
    Url u(doc);
    
    ul.push_back(u.getOnion());
    bson_free (str);
    c++;
    // TCOUT << ".";
    //    bson_destroy (b);
  }
  if (c != urlnb)
    {
      TCOUT("Database now contains " << c << " urls.");
      urlnb = c;
    }
  
  bson_destroy (query);
  mongoc_cursor_destroy (cursor);
  return ul;
}

/** Add the given url to the mongodb database
  *
  * It's not a full Url Object, because, first we add, then we update.
  *
  */
void
Database::upsertUrl(const std::string& url)
{
  if (url.empty()) // Will not upsert an emmpty url
    return;
  
  bson_error_t error;
  bson_oid_t oid;
  bson_t *opts = BCON_NEW("upsert", BCON_BOOL(true));
  
  bson_t *doc = BCON_NEW("url", toBsonStr(url).c_str());

  bson_t *update = BCON_NEW("$set", "{", "url", toBsonStr(url).c_str(), "}");

  if (!mongoc_collection_update_one (col_urls, doc, update,
				     opts, NULL, &error))
    {
      fprintf (stderr, "update failed: %s\n", error.message);
      throw runtime_error(string("Cannot upsert url : ") + error.message);
    }
  
  bson_destroy (doc);
}

/** Set the given url document's crawl time to nom 
  *
  * \return The inserted timestamp fo test purpose
  *
  */
time_t
Database::setCrawlTime(const string& url)
{
  bson_error_t error;
  bson_oid_t oid;
  //  bson_t *opts = NULL; //BCON_NEW("upsert", BCON_BOOL(true));

  time_t T= time(NULL);
  struct  tm tm = *localtime(&T);
  
  bson_t *doc = BCON_NEW("url", toBsonStr(url).c_str());
  bson_t *update = BCON_NEW("$set", "{", "crawled-date",
			    BCON_DATE_TIME (mktime (&tm) * 1000), "}");

  if (!mongoc_collection_update_one (col_urls, doc, update,
				     NULL, NULL, &error))
    {
      fprintf (stderr, "update failed: %s\n", error.message);
      throw runtime_error(string("Can't set crawl time") + error.message);
    }
  
  bson_destroy (doc);
  return T;
}

/** Get a random URL from th database
  *
  *
  */
std::string
Database::getRandomUrl()
{
  auto list = getAllUrls();
  if (list.size() > 0)
    {
      int index = rand() % list.size();
      return list[index];
    }
  else
    return "";
}

void
Database::setTitle(const std::string& url, const std::string& title)
{
  if (title.empty())
    return;

  TCOUT("Found title '" << title << "' for url '" << url << "'");
  
  bson_error_t error;
  bson_oid_t oid;
  //  bson_t *opts = NULL; //BCON_NEW("upsert", BCON_BOOL(true));

  time_t T= time(NULL);
  struct  tm tm = *localtime(&T);
  
  bson_t *doc = BCON_NEW("url", toBsonStr(url).c_str());
  bson_t *update = BCON_NEW("$set", "{", "title", title.c_str(), "}");

  if (!mongoc_collection_update_one (col_urls, doc, update,
				     NULL, NULL, &error))
    {
      fprintf (stderr, "update failed: %s\n", error.message);
      throw runtime_error(string("Can't set title") + error.message);
    }
  
  bson_destroy (doc);
}

/** Increase the death value of a mongodb document identified with url
  *
  *
  *
  */
void
Database::incDeath(const std::string& url)
{
  bson_error_t error;
  bson_t *query = BCON_NEW("url", toBsonStr(url).c_str());
  bson_t *update = BCON_NEW("$inc", "{", "death", BCON_INT32(1) , "}");

  if (!mongoc_collection_update_one (col_urls, query, update,
				     NULL, NULL, &error))
    {
      fprintf (stderr, "update failed: %s\n", error.message);
      throw runtime_error(string("Can't increase death") + error.message);
    }
  
  bson_destroy (query);
}

/** Return a fully deserialized url object objet from its url
  *
  * Note: may return NULL is url not present in database;
  * Note: You may want to delete returned pointer.
  *
  */
Url*
Database::getUrl(const std::string& url)
{
  mongoc_cursor_t *cursor;
  char *str;
  const bson_t *doc;
  bson_t *filter;
  bson_t *opts;
  int64_t count;
  bson_error_t error;
  
  filter = BCON_NEW("url", toBsonStr(url).c_str());
  opts = BCON_NEW ("limit", BCON_INT64 (1));
  //  bson_t* opts = BCON_NEW ("url", BCON_INT32(1), "_id", BCON_INT32(0));
  cursor = mongoc_collection_find_with_opts (col_urls, filter, NULL, NULL);
  int c= 0;
  while (mongoc_cursor_next (cursor, &doc)) {
    str = bson_as_canonical_extended_json (doc, NULL);
    Url* u = new Url(doc);
    return u;
    bson_free (str);
  }
  bson_destroy(filter);
  mongoc_cursor_destroy (cursor);
  return NULL;
}

/** Set the given url to ba back online again afeter a death hiatus
  *
  */
void
Database::setBackOnline(const std::string& url)
{
  Url* uo = getUrl(url); // To get and set DeathNb
  
  bson_error_t error;
  bson_t *query = BCON_NEW("url", toBsonStr(url).c_str());
  bson_t *update = BCON_NEW("$set", "{",
			    "death", BCON_INT32(0) ,
			    "live-back-after", BCON_INT32(uo->getDeathNb()),
			    "}");

  if (!mongoc_collection_update_one (col_urls, query, update,
				     NULL, NULL, &error))
    {
      fprintf (stderr, "update failed: %s\n", error.message);
      throw runtime_error(string("Can't set back online") + error.message);
    }
  bson_destroy (query);

  delete uo;
  uo = NULL;
}

/** Delete the document pointing to the given url
  *
  */
void
Database::deleteUrl(const std::string& url)
{
  bson_t *doc = BCON_NEW("url", toBsonStr(url).c_str());
  bson_error_t error;
  if (!mongoc_collection_remove (col_urls, MONGOC_REMOVE_SINGLE_REMOVE,
				 doc, NULL, &error)) {
    TCOUT("Delete failed: " <<  error.message);
  }

  bson_destroy (doc);
}

void
Database::modifyUrl(const bson_oid_t* id, const string& url)
{
  bson_error_t error;
  bson_oid_t oid;
  //  bson_t *opts = NULL; //BCON_NEW("upsert", BCON_BOOL(true));

  time_t T= time(NULL);
  struct  tm tm = *localtime(&T);
  
  bson_t *doc = BCON_NEW("_id", BCON_OID (id));
  bson_t *update = BCON_NEW("$set", "{", "url", toBsonStr(url).c_str(), "}");

  if (!mongoc_collection_update_one (col_urls, doc, update,
				     NULL, NULL, &error))
    {
      fprintf (stderr, "update failed: %s\n", error.message);
      throw runtime_error(string("Can't modify url") + error.message);
    }
  
  bson_destroy (doc);
}

void
Database::addWords(const std::string& url, const std::list<std::string>& wl)
{
  bson_error_t error;
  bson_t *doc = BCON_NEW("url", toBsonStr(url).c_str());

  // Delete words array : 
  //   db.urls.update( {url: "modify_url_title/"}, { $unset: { words: ""} } )
  bson_t *update = BCON_NEW("$unset", "{", "words", "", "}");
  if (!mongoc_collection_update_one (col_urls, doc, update,
				     NULL, NULL, &error))
    {
      fprintf (stderr, "update failed: %s\n", error.message);
      throw runtime_error(string("Can't unset words : ") +error.message);
    }
  bson_destroy(update);

  // Add words
  bson_oid_t oid;
  int idx = 0;
  for (const auto& s : wl)
    {
      string asciiW = toAscii(s);
      if (asciiW.empty())
	continue;
      
      // Try to increment a sub-variable (in an array)
      string key = string("words.") + asciiW;
      bson_t *update = BCON_NEW("$inc", "{", key.c_str(), BCON_INT32(1) , "}");
      
      if (!mongoc_collection_update_one (col_urls, doc, update,
					 NULL, NULL, &error))
	{
	  fprintf (stderr, "update failed: %s\n", error.message);
	  string msg = "Can't add words for key '" + key + ": '" +
	    error.message;
	  throw runtime_error(msg);
	}
      bson_destroy(update);
      
    }
  bson_destroy(doc);
  if (!test)
    cout << "Added " << wl.size() << " words to db." << endl;
}

/** Delete a document identified by its mongodb oid
  *
  * \param oid The mongodb document's id
  *
  */
void
Database::deleteById(const bson_oid_t* oid)
{
  bson_t *doc = BCON_NEW("_id", BCON_OID (oid));
  bson_error_t error;
  if (!mongoc_collection_remove (col_urls, MONGOC_REMOVE_SINGLE_REMOVE,
				 doc, NULL, &error)) {
    TCOUT("Delete failed: " <<  error.message);
  }
  bson_destroy (doc);
}

list<Word>
Database::getWords(const bson_oid_t* oid)
{

  const bson_t *doc;
  bson_t *query;
  int64_t count;
  bson_error_t error;
  mongoc_cursor_t *cursor;

  list<Word> wl;

  // count
  
  //  bson_t* opts = BCON_NEW ("url", BCON_INT32(1), "_id", BCON_INT32(0));
  query =  BCON_NEW("_id", BCON_OID(oid));
  cursor = mongoc_collection_find_with_opts (col_urls, query, NULL, NULL);
  int c= 0;
  while (mongoc_cursor_next (cursor, &doc))
    {
      bson_iter_t iter;
      bson_iter_t child;
      bson_iter_t number;
      char *json;
      
      if (bson_iter_init_find (&iter, doc, "words") &&
	  BSON_ITER_HOLDS_DOCUMENT (&iter) &&
	  bson_iter_recurse (&iter, &child))
	{
	  while (bson_iter_next (&child))
	    {
	      string key(bson_iter_key(&child));
	      int value = bson_iter_value (&child)->value.v_int32;
	      wl.push_back({key, value});
	    }
	}
    }
  return wl;
}

string
Database::toAscii(const string& s)const
{
  string ret;
  for (auto& c : s)
    if (isalnum(c))
      ret.push_back(c);

  return ret;
}

int
Database::getBlobsNumber(void)
{
  bson_error_t error;
  int64_t count;
  const bson_t* query = bson_new ();
  count = mongoc_collection_count_documents(col_blbs, query, NULL,
					    NULL, NULL, &error);
  if (count < 0) {
    fprintf (stderr, "Count failed: %s\n", error.message);
  }
  return count;
}

/// Insert an URL and its mime-type
void
Database::upsertBlob(const std::string& url, const std::string& type)
{
  bson_error_t error;
  bson_oid_t oid;
  bson_t *opts = BCON_NEW("upsert", BCON_BOOL(true));
  
  bson_t *doc = BCON_NEW("url", toBsonStr(url).c_str());

  bson_t *update = BCON_NEW("$set", "{", "url", toBsonStr(url).c_str(),
			    "mime", type.c_str(),
			    "}");

  if (!mongoc_collection_update_one (col_blbs, doc, update,
				     opts, NULL, &error))
    {
      throw runtime_error(string("Cannot upsert url : ") + error.message);
    }
  
  bson_destroy (doc);
}

/** Delete the document pointing to the given url
  *
  */
void
Database::deleteBlob(const std::string& url)
{
  bson_t *doc = BCON_NEW("url", toBsonStr(url).c_str());
  bson_error_t error;
  if (!mongoc_collection_remove (col_blbs, MONGOC_REMOVE_SINGLE_REMOVE,
				 doc, NULL, &error)) {
    TCOUT("Delete failed: " <<  error.message);
  }

  bson_destroy (doc);
}

int
Database::getDomainsNumber(void)
{
  bson_error_t error;
  int64_t count;
  const bson_t* query = bson_new ();
  count = mongoc_collection_count_documents(col_doms, query, NULL,
					    NULL, NULL, &error);
  if (count < 0) {
    fprintf (stderr, "Count failed: %s\n", error.message);
  }
  return count;
}

void
Database::deleteDomain(const string& dom)
{
  bson_t *doc = BCON_NEW("domain", dom.c_str());
  bson_error_t error;
  if (!mongoc_collection_remove (col_doms, MONGOC_REMOVE_SINGLE_REMOVE,
				 doc, NULL, &error)) {
    TCOUT("Delete failed: " <<  error.message);
  }

  bson_destroy (doc);
}

void
Database::upsertDomain(const string& vdom)
{
  string dom = vdom;
  boost::trim(dom);
  if (dom.empty())
    return;
  
  bson_error_t error;
  bson_oid_t oid;
  bson_t *opts =   BCON_NEW("upsert", BCON_BOOL(true));
  bson_t *doc =    BCON_NEW("domain", dom.c_str());
  bson_t *update = BCON_NEW("$set", "{", "domain", dom.c_str(),
			    "}");

  if (!mongoc_collection_update_one (col_doms, doc, update,
				     opts, NULL, &error))
    {
      throw runtime_error(string("Cannot upsert domain : ") + error.message);
    }
  
  bson_destroy (doc);

}

/** Usage proved bson to have issue with some characters (i.e. [])
  *
  * This will escape these characters with double backslashes (\\).
  *
  */
std::string
Database::toBsonStr(const std::string& s)const
{
  string ret;
  for (auto& c : s)
    {
      if (c == '[' || c == ']')
	ret += "\\" ;
      
      ret.push_back(c);
    }
  
  return ret;
}

/**
  * 
  * No need for a deleteFlags function. Simply set to an empty set.
  *
  */
void
Database::setFlags(Url* urlo, const flags& wl)
{
  // First, remove current flags
  string url = toBsonStr(urlo->getOnion());
  bson_error_t error;
  bson_t *doc = BCON_NEW("url", url.c_str());

  // Delete words array : 
  //   db.urls.update( {url: "modify_url_title/"}, { $unset: { words: ""} } )
  bson_t *update = BCON_NEW("$unset", "{", "flags", "", "}");
  if (!mongoc_collection_update_one (col_urls, doc, update,
				     NULL, NULL, &error))
    {
      fprintf (stderr, "update failed: %s\n", error.message);
      throw runtime_error(string("Can't unset flags : ") +error.message);
    }
  bson_destroy(update);

  // Add words
  bson_oid_t oid;
  int idx = 0;
  for (const auto& s : wl)
    {
      if (s.empty())
	continue;
      
      // Try to increment a sub-variable (in an array)
      bson_t *update = BCON_NEW("$push", "{",
				"flags", s.c_str() ,
				"}");
      
      if (!mongoc_collection_update_one (col_urls, doc, update,
					 NULL, NULL, &error))
	{
	  fprintf (stderr, "update failed: %s\n", error.message);
	  string msg = "Can't add words for url '" + url + ": '" +
	    error.message;
	  throw runtime_error(msg);
	}
      bson_destroy(update);
      
    }

  // Set flag type
  bson_t *update2 = BCON_NEW("$set", "{",
			    "flag-type", "auto",
			    "}");

  if (!mongoc_collection_update_one(col_urls, doc, update2, NULL,NULL,&error))
      throw runtime_error(string("Cannot set url flag-type: ") + error.message);
  
  bson_destroy (doc);
}

flags
Database::getFlags(Url*u) 
{
  const bson_t *doc;
  bson_t *query;
  int64_t count;
  bson_error_t error;
  mongoc_cursor_t *cursor;

  flags fl;

  //  bson_t* opts = BCON_NEW ("url", BCON_INT32(1), "_id", BCON_INT32(0));
  query =  BCON_NEW("_id", BCON_OID(u->getId()));
  cursor = mongoc_collection_find_with_opts (col_urls, query, NULL, NULL);
  int c= 0;
  while (mongoc_cursor_next (cursor, &doc))
    {
      bson_iter_t iter;

      if (bson_iter_init_find (&iter, doc, "flags") &&
	  BSON_ITER_HOLDS_ARRAY (&iter))
	{

	  bson_t *arr;
	  const uint8_t *data = NULL;
	  uint32_t len = 0;
	  
	  bson_iter_array (&iter, &len, &data);
	  arr = bson_new_from_data (data, len);

	  bson_iter_t child;
	  bson_iter_init(&child, arr);
	  while (bson_iter_next(&child)) {
	    const bson_value_t * value = bson_iter_value (&child);
	    string val((char*)value->value.v_utf8.str);
	    fl.insert(val);
	  }
	  
	  bson_destroy (arr);
	}   
    }
  return fl;
}

/**
  * 
  * No need for a deleteFlags function. Simply set to an empty set.
  *
  */
void
Database::setFlags(Domain* domo, const flags& wl)
{
  // First, remove current flags
  string url = toBsonStr(domo->getDomain());
  bson_error_t error;
  bson_t *doc = BCON_NEW("domain", domo->getDomain().c_str());

  // Delete words array : 
  //   db.urls.update( {url: "modify_url_title/"}, { $unset: { words: ""} } )
  bson_t *update = BCON_NEW("$unset", "{", "flags", "", "}");
  if (!mongoc_collection_update_one (col_doms, doc, update,
				     NULL, NULL, &error))
    {
      fprintf (stderr, "update failed: %s\n", error.message);
      throw runtime_error(string("Can't unset flags : ") +error.message);
    }
  bson_destroy(update);

  // Add words
  bson_oid_t oid;
  int idx = 0;
  for (const auto& s : wl)
    {
      if (s.empty())
	continue;
      
      // Try to increment a sub-variable (in an array)
      bson_t *update = BCON_NEW("$push", "{",
				"flags", s.c_str() ,
				"}");
      
      if (!mongoc_collection_update_one (col_doms, doc, update,
					 NULL, NULL, &error))
	{
	  fprintf (stderr, "update failed: %s\n", error.message);
	  string msg = "Can't add words for url '" + url + ": '" +
	    error.message;
	  throw runtime_error(msg);
	}
      bson_destroy(update);
      
    }

  // Set flag type
  bson_t *update2 = BCON_NEW("$set", "{",
			    "flag-type", "auto",
			    "}");

  if (!mongoc_collection_update_one(col_doms, doc, update2, NULL,NULL,&error))
      throw runtime_error(string("Cannot set url flag-type: ") + error.message);
  
  bson_destroy(doc);
}

flags
Database::getFlags(Domain* d) 
{
  const bson_t *doc;
  bson_t *query;
  int64_t count;
  bson_error_t error;
  mongoc_cursor_t *cursor;

  flags fl;

  //  bson_t* opts = BCON_NEW ("url", BCON_INT32(1), "_id", BCON_INT32(0));
  query = BCON_NEW("domain", d->getDomain().c_str());
  cursor = mongoc_collection_find_with_opts (col_doms, query, NULL, NULL);
  int c= 0;
  while (mongoc_cursor_next (cursor, &doc))
    {
      bson_iter_t iter;

      if (bson_iter_init_find (&iter, doc, "flags") &&
	  BSON_ITER_HOLDS_ARRAY (&iter))
	{

	  bson_t *arr;
	  const uint8_t *data = NULL;
	  uint32_t len = 0;
	  
	  bson_iter_array (&iter, &len, &data);
	  arr = bson_new_from_data (data, len);

	  bson_iter_t child;
	  bson_iter_init(&child, arr);
	  while (bson_iter_next(&child)) {
	    const bson_value_t * value = bson_iter_value (&child);
	    string val((char*)value->value.v_utf8.str);
	    fl.insert(val);
	  }
	  
	  bson_destroy (arr);
	}   
    }
  return fl;
}

/** Returns a Domain object pointer from its name
  *
  */
Domain*
Database::getDomain(const std::string& domo)
{
  const bson_t *doc;
  bson_t *query = BCON_NEW("domain", domo.c_str());
  mongoc_cursor_t* cursor;
  
  cursor = mongoc_collection_find_with_opts (col_doms, query, NULL, NULL);
  int c= 0;
  if (mongoc_cursor_next (cursor, &doc))
    {
      return new Domain(doc);
    }
  else
    return NULL;
  
}
