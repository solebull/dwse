#ifndef __RENAME_EXTRACT_HPP__
#define __RENAME_EXTRACT_HPP__

#include "CleanerCommand.hpp"

/** Try to extract url from a non-valid scheme-based string
  *
  *
  *
  */
class ccRenameExtract : public CleanerCommand
{
public:
  ccRenameExtract();
  virtual int run(Database* db);
  
protected:
  std::string remove(const std::string&);
};


#endif // !__RENAME_EXTRACT_HPP__
