const puppeteer = require('puppeteer');

async function run() {
    // { headless: false },
    let browser = await puppeteer.launch({				
	args: [
	    '--proxy-server=socks5://127.0.0.1:9050', // Using tor
	    '--no-sandbox',
	    '--disable-setuid-sandbox'
	],
    });
    let page = await browser.newPage();
    var onion='http://kaarvixjxfdy2wv2.onion';
    await page.goto(onion);
    await page.screenshot({ path: './image.jpg', type: 'jpeg' });
    await page.close();
    await browser.close();
}

run();
