#include "CleanerCommand.hpp"

#include <vector>
#include <string>

// Forward declaration
class Database;
// End of forward declaration

class ccListDuplicate : public CleanerCommand
{
public:
  ccListDuplicate();
  virtual int run(Database* db);

protected:
  int count(std::vector<std::string> li, std::string u);
};
