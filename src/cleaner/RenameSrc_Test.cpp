#include "RenameSrc.hpp"

#include <gtest/gtest.h>

using namespace std;

class ccRenameSrcTest : public ccRenameSrc
{
public:
  string _remove(const string& yu){  return remove(yu); }
};

TEST( ccRenameSrc, remove )
{
  string o = "azeaze";
  ccRenameSrcTest r;
  
  ASSERT_EQ(r._remove("aze"), "aze");
  ASSERT_EQ(r._remove("src='aze'"), "aze");
}
