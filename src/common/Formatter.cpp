#include "Formatter.hpp"

#include <stdexcept>
#include <iostream>
#include <sstream>

#include <iomanip>     // USES std::setprecision()
#include <chrono>

#include <signal.h>    // USES signal()
#include <sys/ioctl.h> // USES ioctl()


using namespace std;


// We have to keep these values global to be able to capture them
// in the signal handling lambda.
struct winsize winsz;               //!< Window size structure
bool           winsz_changed=false; //!< Has the window size structure change ?

// init Should we init ncurses
Formatter::Formatter(bool ncursesInit):
  wmain(NULL),
  wstatus(NULL),
  usecolors(true),
  indexed_urls(0),    mod_indexed_urls(0),
  indexed_domains(0), mod_indexed_domains(0),
  maxMainHeight(0),
  wrow(0), wcol(0)
{

  if (ncursesInit)
    init();

  // Initial window size
  ioctl(0, TIOCGWINSZ, &winsz);
  winsz_changed=true;
  
  // Register signal handler (from https://stackoverflow.com/a/11468500 and
  // http://www.rkoucha.fr/tech_corner/sigwinch.html)
  signal(SIGWINCH, [](int signum)
  {
    // Terminal was resized
    if (SIGWINCH == signum)
      {
	//	struct winsize winsz;
	
	ioctl(0, TIOCGWINSZ, &winsz);
	winsz_changed=true;
	/*	printf("SIGWINCH raised, window size: %d rows / %d columns\n",
	       winsz.ws_row, winsz.ws_col);
	*/
      }
  });
  
}


Formatter::~Formatter()
{
  
}

void
Formatter::init()
{
  auto w = initscr();
  noecho();
  nodelay(w, TRUE);
  timeout(1);
  if (has_colors() == FALSE) {
    endwin();
    printf("WARNING: Your terminal does not support color\n");
    usecolors = false;
  }
  else
    {
      init_pair(1, COLOR_BLACK, COLOR_WHITE);
      init_pair(2, COLOR_YELLOW, COLOR_GREEN);
    }
  refresh();

  // Windows
  wmain = newwin( LINES - 4, COLS, 0,  0);
  maxMainHeight = LINES - 6;
  box(wmain, ACS_VLINE, ACS_HLINE);
  wstatus = newwin( 3, COLS, LINES - 4, 0);
  box(wstatus, ACS_VLINE, ACS_HLINE);

  //  wrefresh(wmain);
  wrefresh(wstatus);
}


/** Run one frame
  *
  * \param keycode Will contain possible pressed key code at each call or -1
  *        if none was pressed.
  *
  * \return 0 to continue, any other value to stop.
  *
  */
int
Formatter::run(int* keycode)
{
  update();

  int ch = getch();
  *keycode = ch;
  if (ch == 113 || ch == 81) // q||Q
    {
      endwin();
      return -1;
    }
  else
    {
      string s = " - " + std::to_string(ch);
      MainListItem* i= new MainListItem();
      i->url = s;
      add(i);
      int y = 19;
      //      for (auto l : mainList)
      std::list<MainListItem*>::reverse_iterator rit;
      for (rit=mainList.rbegin(); rit!=mainList.rend(); ++rit)
	{
	  mvwprintw(wmain, --y, 1, (*rit)->url.c_str());
	  if (y<1) break;
	}
      
      wrefresh(wmain);
      return 0;
    }
  return -1;
}


/** Update the screen for this frame
  *
  * This will update all status values (url ans domain) as the time.
  *
  *
  */
void
Formatter::update(void)
{
  
  if (winsz_changed)
    handleResize();
  
  addUrls(1);
  
  // Urls
  mvwprintw(wstatus, 1, 2, "Urls: %s (+%s)",
	    intToStr(indexed_urls).c_str(),
	    intToStr(mod_indexed_urls).c_str() );
  
  mvwprintw(wstatus, 1, 28, "| Domains: %s (+%s)",
	    intToStr(indexed_domains).c_str(),
	    intToStr(mod_indexed_domains).c_str() );
  
  // Elapsed time
  auto end = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = end - start;
  
  mvwprintw(wstatus, 1, wcol-23, "| Ela: %s", elaToStr(diff.count()).c_str());

  if (wrefresh(wstatus) == ERR)
    throw std::runtime_error("wrefresh failed");
  
}

/** Add url number to both indexed and modified ones 
  *
  * \param u Number of URLs to add.
  *
  */
void
Formatter::addUrls(int u)
{
  indexed_urls     += u;
  mod_indexed_urls += u;
}

/** Add domain number to both indexed and modified ones 
  *
  * \param d Number of domains to add.
  *
  */
void
Formatter::addDomains(int d)
{
  indexed_domains    += d;
  mod_indexed_domains+= d;
}

/** Add the given item to the main list
  *
  * \param mli The item to be added.
  *
  */
void
Formatter::add(MainListItem* mli)
{
  mainList.push_back(mli);
  if (mainList.size() > maxMainHeight)
    mainList.pop_front();
}

/** Return as a string a milliseconds-based elapsed time
  *
  * \param diff The elapsed milliseconds from start.
  *
  * \return a string.
  *
  */
std::string
Formatter::elaToStr(double diff)
{
  using namespace std::chrono;
  std::chrono::duration<double> d(diff);
 
  ostringstream oss;
  int precision = 4;

  auto hhh = duration_cast<hours>(d);
  d -= hhh;
  auto mm = duration_cast<minutes>(d);
  d -= mm;
  auto ss = duration_cast<seconds>(d);
  d -= ss;
  auto ms = duration_cast<milliseconds>(d);
  
  std::ostringstream stream;
  stream << std::setfill('0') << std::setw(3) << hhh.count() << ':' <<
    std::setfill('0') << std::setw(2) << mm.count() << ':' << 
    std::setfill('0') << std::setw(2) << ss.count() << '.' <<
    std::setfill('0') << std::setw(3) << ms.count();

  std::string result = stream.str();
  return result;
}

/** Handles printing big number up to yotta (Y)
  *
  * \param i The number to be handled.
  *
  * \return the string with two decimals and unit.
  *
  */
std::string
Formatter::intToStr(unsigned int i) const
{
  ostringstream oss;
  oss << std::fixed;
  oss << std::setprecision(2);

  // https://simple.wikipedia.org/wiki/Names_for_large_numbers#Names_for_large_numbers
  if (i>1'000'000'000'000'000)
    {
      double d=i/1'000'000'000'000'000.0f;
      oss << d << "Y";
      return oss.str();
    }
  else if (i>1'000'000'000'000)
    {
      double d=i/1'000'000'000'000.0f;
      oss << d << "T";
      return oss.str();
    }
  else if (i>1'000'000'000)
    {
      double d=i/1'000'000'000.0f;
      oss << d << "G";
      return oss.str();
    }
  else if (i>1'000'000)
    {
      double d=i/1'000'000.0f;
      oss << d << "M";
      return oss.str();
    }
  else if (i>1'000)
    {
      double d=i/1'000.0f;
      oss << d << "K";
      return oss.str();
    }
  
  return std::to_string(i);
}

/** Print actual terminal size (in row/column) to standard output
  *
  */
void
Formatter::dumpSize(void)
{
  cerr << "Initial window size " + std::to_string(wrow) +
    "-" + std::to_string(wcol) << endl;
}


/** Handle the resize signal logic
  *
  * We have to call this when the global variable winsz_changed is true.
  * It is called from the update() function.
  *
  */
void
Formatter::handleResize(void)
{
  wrow=winsz.ws_row;
  wcol=winsz.ws_col;
  dumpSize();
  winsz_changed=false;

  // Resize needed windows (lines, cols)
  wresize(wstatus, 3, wcol);
  mvwin(wstatus, wrow-4, 0);
  wclear(wstatus);
  box(wstatus, ACS_VLINE, ACS_HLINE);
}
