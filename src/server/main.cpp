#include <microhttpd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string>

using namespace std;

static struct MHD_Daemon *server_daemon;

int answer_to_connection (void *cls, struct MHD_Connection *connection,
                          const char *url,
                          const char *method, const char *version,
                          const char *upload_data,
                          size_t *upload_data_size, void **con_cls)
{
  string page = "<aze>";
  struct MHD_Response *response;
  response = MHD_create_response_from_buffer (page.size(),
					      (void*) page.c_str(),
					      MHD_RESPMEM_PERSISTENT);
  int ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
  MHD_destroy_response (response);
  return ret;
}

// Return -1 in case of error
int
server_start(uint16_t port)
{
  printf("Starting webserver listening on port http://localhost:%d\n", port);
  server_daemon = MHD_start_daemon (MHD_USE_INTERNAL_POLLING_THREAD, port,
                                    NULL, NULL,
                             &answer_to_connection, NULL, MHD_OPTION_END);
  if (NULL == daemon)
    return -1;
}

void server_stop()
{
  MHD_stop_daemon (server_daemon);

}

int
main(void)
{
  server_start(1011);
  while(true)
    {

    }
  server_stop();
}
