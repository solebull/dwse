var express = require('express');
var router = express.Router();

const Database = require("../db");
var isQueryEmpty = require('../query');

var results;

function samples()
{
    return [
	{
	    title: "Aze",
	    url: "http://lkljdflk",
	    category: "random",
	    flags: ["nsfw"]
	},
	{
	    title: "zer",
	    url: "http://lkljdflk",
	    category: "blog",
	    flags: ["nsfl"]
	},
    ];
}

router.get('/', function(req, res, next) {
    var queryStr = '';
    if (req.query === 'undefined')
	queryStr = 'undefined';
    else
	queryStr = req.query.q;

    if (isQueryEmpty(queryStr))
	res.render('index', { title: 'dwse', query: ''});
    else
	run(queryStr, res);
    
//    results = samples();
    console.log("RESULTS: "+results);
});

function showResults(arr) {

    arr.forEach(function(a) {
	console.log("  " + a.title);
    });
}

async function run(queryStr, res) {
    try {
	if (queryStr === 'undefined' || queryStr === '')
	    return ;

	var db = new Database(false);
/*	await client.connect();
	const database = client.db("dwse");
*/
//	const urls = database.collection("urls");
	// Query for a movie that has the title 'The Room'
	var query = {};
	query["words."+queryStr] =  { $gt: 0 };
	var sort = {};
	sort["words."+queryStr] =  -1;
	
	console.log("QUERY: " + query);
	// Next was queryStr
	var pres = db.p_query(query, {url: 1, title: 1}, sort);
	console.log("PRES+" + JSON.stringify(pres));
	pres.then(function(results) {
	    console.log("RESULTS for "+queryStr+" :" );
//	    showResults(results);
	    if (res)
		res.render('index', { title: 'dwse', query: queryStr,
				      results: results});
	});
    } finally {
	//await client.close();
    }
}
run().catch(console.dir);
module.exports = router;
