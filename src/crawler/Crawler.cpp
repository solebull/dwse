#include "Crawler.hpp"

#include <cstdlib>
#include <iostream>
#include <exception>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <boost/algorithm/string/trim.hpp>

#include "Logger.hpp"
#include "MimeType.hpp"

#include "common/Database.hpp"
#include "common/Url.hpp"
#include "common/Config.hpp"

using namespace std;

string fout_name = "crawl-result.cpp";

/// Empty constructor for tests only
Crawler::Crawler()
{

}

Crawler::Crawler(Config* c, Database* db):
  cfgcpy(c),
  dbcpy(db)
{
  cout << "Checking if processor is available...";
  if (system(NULL))
    cout << ("Ok") << endl;
  else
    throw runtime_error("Can't use cstdlib's system function");


  fout.open(fout_name, std::ios_base::out);
  if (!fout.is_open()) {
    throw runtime_error("failed to open '" + fout_name + "'");
  }
  
  fout << "#include <list>" << endl;
  fout << "#include <string>" << endl;
  fout << "using namespace std;" << endl;
}

Crawler::~Crawler()
{
  dbcpy = NULL;  // We don't own database memory
  fout.close();
}

/** Crawl the given url
  *
  *
  *
  */
void
Crawler::crawl(std::string url)
{
  if (!isAlive(url))
    {
      cout << " D " << endl;
      dbcpy->incDeath(url);
      return;
    }

  if (Url::hasAnchor(url))
    {
      cout << " # " << endl;
      return;
    }
  
  // Can connect : is back after at least one death ?
  Url* uo = dbcpy->getUrl(url);
  if (uo){
    if (uo->getDeathNb() > 0)
      {
	// Is back online
	cout << " B ";
	dbcpy->setBackOnline(url);
      }
    else
      {
	cout << " A ";
      }
    delete uo;
    uo = NULL;
  }
      
  // Download
  cout << "Crawling...";
  cout.flush();
  string s = "torify wget -a wget.logfile -O out " + escapeUrl(url);
  int ret = system(s.c_str());
  if (ret == -1)
    {
      cout << "Error when crawling URL (-1)" << endl;
      return;
    }
  else if (ret == 2 || ret == 1024)
    {
      dbcpy->incDeath(url);
      return;
    }
  cout << " done (system return status=" << ret << ")" << endl;

  // Get mime type and add as blob if non-html
  MimeType m("out");
  if (!m.isHtml())
    {
      string mt = m.getMimeType();
      cout << "Adding as blob (" << mt << ")" << endl;
      dbcpy->upsertBlob(url, mt);
      return;
    }
  else
    {   // Finally add the URL
      dbcpy->upsertUrl(url);
      dbcpy->upsertDomain(Url::extractDomain(url));
    }
  
  // Parse : just search for .onion text
  ifstream file;
  ostringstream oss;
  ifstream myfile ("out");
  if (myfile.is_open())
  {
    string line;
    while ( getline (myfile,line) )
      {
	oss << line;
      }
    myfile.close();
  }
  string content =  oss.str();

  // Early exit if no URL found
  if (content.find(".onion") == string::npos)
    return ;

  // Parsing
  cout << "Parsing ...";
  cout << "  " << content.length() << " bytes/";
  auto parsable = istringstream(removeMarkup(content));
  string parsed;
  int words_found = 0;
  list<string> words;
  while (parsable >> parsed)
    {
      if (keepWord(parsed))
	{
	  words.push_back(parsed);
	  ++words_found;
	}
    }
  cout << "Adding '" << words_found << "' to url " << url << endl;
  dbcpy->addWords(url, words);
  cout << endl;
  cout.flush();

  // Remove some chars that prevent correct parsing
  std::replace( content.begin(), content.end(), '"', ' ');
  std::replace( content.begin(), content.end(), '<', ' ');
  std::replace( content.begin(), content.end(), '>', ' ');
  std::replace( content.begin(), content.end(), '(', ' ');
  std::replace( content.begin(), content.end(), ')', ' ');

  fout << "list<string> getOnions() {" << endl;
  fout << "list<string> onions;" << endl;
  
  string delimiter = " ";  
  size_t pos = 0;
  std::string token;
  size_t found = 0;
  while ((pos = content.find(delimiter)) != std::string::npos) {
    token = content.substr(0, pos);
    boost::trim(token);
    if (token.find(".onion") != string::npos
	&& token.length() > string(".onion").length())
      {
	found++;
	fout << "onions.push_back(\"" << token << "\");" << endl;
	dbcpy->upsertUrl(token);
      }
    content.erase(0, pos + delimiter.length());
  }
  content =  oss.str();  // erase has removed all content
  std::cout << "  Found "<< found << " non-unique items" << std::endl;

  fout << " return onions; }" << endl;
  dbcpy->setTitle(url, extractTitle(content));
  dbcpy->setCrawlTime(url);

  // Flags
  flags fl;
  Url* uo2 = dbcpy->getUrl(url);
  if (isNsfw(words))
    {
      cout << "is *NSFW* ";
      fl.insert("nsfw");
    }

  if (isNsfl(words))
    {
      cout << "is *NSFL* ";
      fl.insert("nsfl");
    }
  dbcpy->setFlags(uo2, fl); 
}

/** Extract the webpage title from its content
  *
  */
string
Crawler::extractTitle(const std::string& s)
{
  // To lower case to handle TITLE case etc..
  string tolower = s;
  std::for_each(tolower.begin(), tolower.end(), [](char & c) {
        c = ::tolower(c);
    });

  size_t pos = tolower.find("<title>");
  size_t pos2 = tolower.find("</title>");

  if (pos == string::npos && pos2 == string::npos)
    return "";

  pos += 7; // The '<title>' size

  string title = s.substr(pos, pos2 - pos);
  boost::trim(title);
  return title;
}

/** Check if an URL is alive
  *
  */
bool
Crawler::isAlive(const std::string& url)
{
  LT(truncateUrl(url, 20));
  // Double quotes are used to avoid issue with '&amp;' urls
  string s = "torify httping -c 1 " + escapeUrl(url) + REDIRECT_FILE;
  int ret = system(s.c_str());
  if (ret == 0)
    return true;
  else
    return false;
}

string
Crawler::escapeUrl(const string& o)
{
  string s = "\"";
  return s + o + "\"";
}

/** Remove occurnces of words known as markup 
  *
  */
std::string
Crawler::removeMarkup(const string& o)
{
  // Mainly from https://developer.mozilla.org/fr/docs/Web/HTML/Element
  list<string> tbr =
    {
     "<html>", "</html>",
     "<base>", "</base>",
     "<div>", "</div>",
     "<span>", "</span>",

     "<head>", "</head>",
     "<link>", "</link>",
     "<meta>", "</meta>",
     "<style>", "</style>",
     "<title>", "</title>",

     "<body>", "</body>",
     "<address>", "</address>",
     
     "<article>", "</article>",
     "<aside>", "</aside>",
     
     "<footer>", "</footer>",
     "<header>", "</header>",
     
     "<h1>", "</h1>",
     "<h2>", "</h2>",
     "<h3>", "</h3>",
     "<h4>", "</h4>",
     "<h5>", "</h5>",
     "<h6>", "</h6>",
     
     "<main>", "</main>",
     "<nav>", "</nav>",
     "<section>", "</section>",
     
     "<blockquote>", "</blockquote>",
     "<dd>", "</dd>",
     "<div>", "</div>",
     "<dl>", "</dl>",
     "<dt>", "</dt>",
     "<figcaption>", "</figcaption>",

     "<figure>", "</figure>",
     "<hr>", "</hr>",
     "<li>", "</li>",
     "<ol>", "</ol>",
     "<p>", "</p>",
     
     "<pre>", "</pre>",
     "<ul>", "</ul>",
     
     "<a>", "</a>",
     "<abbr>", "</abbr>",
     "<b>", "</b>",
     "<bdi>", "</bdi>",
     
     "<bdo>", "</bdo>",
     "<br>", "</br>",
     "<cite>", "</cite>",
     "<code>", "</code>",
     
     "<data>", "</data>",
     "<dfn>", "</dfn>",
     
     "<em>", "</em>",
     "<i>", "</i>",
     "<kbd>", "</kbd>",
     
     "<mark>", "</mark>",
     "<q>", "</q>",
     "<rb>", "</rb>",
     "<rp>", "</rp>",
     "<rt>", "</rt>",

     "<rtc>", "</rtc>",
     "<ruby>", "</ruby>",
     "<s>", "</s>",
     "<samp>", "</samp>",
     
     "<small>", "</small>",
     "<span>", "</span>",
     "<strong>", "</strong>",
     "<sub>", "</sub>",
     "<sup>", "</sup>",
     "<time>", "</time>",
     "<u>", "</u>",
     "<var>", "</var>",
     "<wbr>", "</wbr>",

     "<area>", "</area>",
     "<audio>", "</audio>",
     "<img>", "</img>",
     "<map>", "</map>",
     "<track>", "</track>",

     "<video>", "</video>",
     "<embed>", "</embed>",
     "<iframe>", "</iframe>",
     "<object>", "</object>",

     "<param>", "</param>",
     "<picture>", "</picture>",
     "<portal>", "</portal>",
     "<source>", "</source>",
     "<canvas>", "</canvas>",
     "<noscript>", "</noscript>",
     "<script>", "</script>",
     
     "<del>", "</del>",
     "<ins>", "</ins>",

     "<caption>", "</caption>",
     "<col>", "</col>",
     "<colgroup>", "</colgroup>",
     "<table>", "</table>",
     "<tbody>", "</tbody>",
     "<td>", "</td>",
     "<tfoot>", "</tfoot>",
     "<th>", "</th>",
     "<thead>", "</thead>",
     "<tr>", "</tr>",

     "<button>", "</button>",
     "<datalist>", "</datalist>",
     "<fieldset>", "</fieldset>",
     "<form>", "</form>",
     "<input>", "</input>",
     "<label>", "</label>",
     "<legend>", "</legend>",
     "<meter>", "</meter>",
     "<optgroup>", "</optgroup>",
     "<option>", "</option>",
     "<output>", "</output>",
     "<progress>", "</progress>",
     "<select>", "</select>",
     "<textarea>", "</textarea>",
     
     "<details>", "</details>",
     "<dialog>", "</dialog>",
     "<menu>", "</menu>",
     "<summary>", "</summary>",

     "<content>", "</content>",
     "<shadow>", "</shadow>",    // obsolete
     "<slot>", "</slot>",
     "<template>", "</template>",

     // All obsoletes or deprecated
     "<acronym>", "</acronym>",
     "<applet>", "</applet>",
     "<basefont>", "</basefont>",
     "<bgsound>", "</bgsound>",
     "<big>", "</big>",
     "<blink>", "</blink>",
     "<center>", "</center>",
     "<content>", "</content>",
     "<dir>", "</dir>",
     "<font>", "</font>",
     "<frame>", "</frame>",
     "<frameset>", "</frameset>",
     "<hgroup>", "</hgroup>",
     "<image>", "</image>",
     "<isindex>", "</isindex>",
     "<keygen>", "</keygen>",
     "<listing>", "</listing>",
     "<marquee>", "</marquee>",
     "<menuitem>", "</menuitem>",
     "<multicol>", "</multicol>",
     "<nextid>", "</nextid>",
     "<nobr>", "</nobr>",
     "<noembed>", "</noembed>",
     "<noframes>", "</noframes>",
     "<plaintext>", "</plaintext>",
     "<rb>", "</rb>",
     "<rtc>", "</rtc>",
     "<shadow>", "</shadow>",
     "<spacer>", "</spacer>",
     "<strike>", "</strike>",
     "<tt>", "</tt>",
     "<xmp>", "</xmp>",
     ",", ";", ":", "!", "="
    };

  
  string ret = o;
  for (auto& t : tbr)
    {
      size_t pos = ret.find(t);
      while (pos != std::string::npos)
	{
	  ret.erase(pos, t.length());
	  pos = ret.find(t);
	}
    }

  std::replace( ret.begin(), ret.end(), ',', ' ');
  std::replace( ret.begin(), ret.end(), ';', ' ');
  std::replace( ret.begin(), ret.end(), ':', ' ');
  std::replace( ret.begin(), ret.end(), '!', ' ');
  std::replace( ret.begin(), ret.end(), '&', ' ');
  std::replace( ret.begin(), ret.end(), '%', ' ');
  std::replace( ret.begin(), ret.end(), '#', ' ');
  std::replace( ret.begin(), ret.end(), '"', ' ');
  std::replace( ret.begin(), ret.end(), '\'', ' ');
  std::replace( ret.begin(), ret.end(), '\\', ' ');
  std::replace( ret.begin(), ret.end(), '-', ' ');
  std::replace( ret.begin(), ret.end(), '_', ' ');
  std::replace( ret.begin(), ret.end(), '$', ' ');
  std::replace( ret.begin(), ret.end(), '/', ' ');
  std::replace( ret.begin(), ret.end(), '*', ' ');
  std::replace( ret.begin(), ret.end(), '+', ' ');
  std::replace( ret.begin(), ret.end(), '.', ' ');
  std::replace( ret.begin(), ret.end(), '<', ' ');
  std::replace( ret.begin(), ret.end(), '>', ' ');
  std::replace( ret.begin(), ret.end(), '(', ' ');
  std::replace( ret.begin(), ret.end(), ')', ' ');
  std::replace( ret.begin(), ret.end(), '?', ' ');
  std::replace( ret.begin(), ret.end(), '[', ' ');
  std::replace( ret.begin(), ret.end(), ']', ' ');


  return ret;
}

bool
Crawler::keepWord(const std::string& w)
{
  if (w.empty())
    return false;
  
  if (w.size() < 4)
    return false;

  return true;
}

/** Is the extracted words NSFW 
  *
  */
bool
Crawler::isNsfw(const std::list<std::string>& words)
{
  FlagConfig nsfw = cfgcpy->getNsfwFlagConfig();
  int i = 0;
  for (auto& a : words)
    {
      ++i;
      if (i > nsfw.threshold)
	break;

      for (auto& w : nsfw.words)
	if (a == w)
	  return true;
    }
  return false;
}

/** Is the extracted words NSFW 
  *
  */
bool
Crawler::isNsfl(const std::list<std::string>& words)
{
  FlagConfig nsfl = cfgcpy->getNsflFlagConfig();
  int i = 0;
  for (auto& a : words)
    {
      ++i;
      if (i > nsfl.threshold)
	break;

      for (auto& w : nsfl.words)
	if (a == w)
	  return true;
    }
  return false;
}

string
Crawler::truncateUrl(const string& str, int width, bool show_ellipsis)
{
  if (str.length() > width)
    if (show_ellipsis)
      return str.substr(0, width - 3) + "...";
    else
      return str.substr(0, width);
  return str;
}
