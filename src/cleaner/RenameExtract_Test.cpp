#include "RenameExtract.hpp"

#include <gtest/gtest.h>

using namespace std;

class ccRenameExtractTest : public ccRenameExtract
{
public:
  string _remove(const string& yu){  return remove(yu); }
};

TEST( ccRenameExtract, remove )
{
  string o = "azeazehttp://khjqsjkqsh.onionjklsdlfj";
  ccRenameExtractTest r;
  
  ASSERT_EQ(r._remove(o), "http://khjqsjkqsh.onion");
  ASSERT_EQ(r._remove("window=https://eh.onionaze"), "https://eh.onion");
}
