#ifndef __CRAWLER_HPP__
#define __CRAWLER_HPP__

#include <string>
#include <list>
#include <fstream>

// Forward declarations
class Config;
class Database;
// End of forward declarations

class Crawler
{
public:
  Crawler();
  Crawler(Config*, Database*);
  ~Crawler();

  void crawl(std::string);

protected:
  std::string extractTitle(const std::string&);
  bool        isAlive(const std::string&);
  bool        keepWord(const std::string&);
  std::string escapeUrl(const std::string& o);
  std::string removeMarkup(const std::string& o);
  std::string truncateUrl(const std::string&, int width=10,
			  bool show_ellipsis=true);
  
  // Flags
  bool isNsfw(const std::list<std::string>&);
  bool isNsfl(const std::list<std::string>&);
  
private:
  std::ofstream fout;
  Config*   cfgcpy;
  Database* dbcpy;
};

#endif // ! __CRAWLER_HPP__
