#include "MimeType.hpp"

#include <iostream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

using namespace std;

/** Try to detect the mime-type of the given file
  *
  */
MimeType::MimeType(const std::string& filen):
  filename(filen)
{

  string cmd = "file --mime-type --brief " + filename;
  string mt =  exec(cmd);

  if (mt.rfind("cannot open `", 0) == 0)
    {
      throw runtime_error("Can't find file " + filen);
    }
  
  mimetype = mt;

  // Remove trailing newline
  if ((int)mt.back() == 10)
    mimetype.pop_back();
}


const std::string&
MimeType::getFilename(void) const
{
  return filename;
}

const std::string&
MimeType::getMimeType(void) const
{
  return mimetype;
}

string
MimeType::exec(const string& cmd)
{
  std::array<char, 128> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&pclose)>
    pipe(popen(cmd.c_str(), "r"), pclose);
  
  if (!pipe) {
    throw std::runtime_error("popen() failed!");
  }
  while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
    result += buffer.data();
  }
  return result;
}

bool
MimeType::isHtml(void)
{
  if (mimetype == "text/html")
    return true;
  else
    return false;
}
