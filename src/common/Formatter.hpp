#ifndef __FORMATTER_HPP__
#define __FORMATTER_HPP__

#include <ncurses.h>

#include <list>
#include <string>
#include <chrono>

/** An item of the Fiormatter main window
  *
  *
  */
struct MainListItem
{
  std::string url;     //!< The crawled url
};

/** Ncurses-based screen formatter for crawler
  *
  * Used to print multiple MainListItem known as mainList.
  *
  */
class Formatter
{
public:
  typedef unsigned long count;
  
  Formatter(bool ncursesInit=true);
  ~Formatter();

  int run(int*);

  void addUrls(int);
  void addDomains(int);

  void add(MainListItem*);
  
protected:
  void init();
  void update(void);
  std::string elaToStr(double);
  std::string intToStr(unsigned int) const;

  void handleResize(void);
  void dumpSize(void);
  
private:
  // Windows && ncurses pointers
  WINDOW* wmain;              //!< The main window
  WINDOW* wstatus;            //!< The status window

  bool usecolors;             //!< Can we use terminal colors
  
  // Counts
  count indexed_urls;         //!< Number of indexed urls
  count mod_indexed_urls;     //!< New indexed urls this session

  count indexed_domains;      //!< Number of indexed domains
  count mod_indexed_domains;  //!< New indexed domains this session

  // Content
  std::list<MainListItem*> mainList; //!< The main window content
  int maxMainHeight;
  
  // Time
  std::chrono::time_point<std::chrono::steady_clock> start;

  int wrow, wcol; //!< Window size
};

#endif // !__FORMATTER_HPP__
