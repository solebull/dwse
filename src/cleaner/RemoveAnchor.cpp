#include "RemoveAnchor.hpp"

#include "common/Database.hpp"
#include "common/Url.hpp"

#include <iostream>
#include <algorithm> // USES std::find()

using namespace std;

ccRemoveAnchor::ccRemoveAnchor()
  : CleanerCommand("rm-anchor", "Remove url containing anchor")
{
  
}
  
int
ccRemoveAnchor::run(Database* db)
{
  // Modified records
  int mod = 0;
  std::vector<std::string> urls = db->getAllUrls();

  for (auto& u : urls)
    {
      if (Url::hasAnchor(u))
	{
	  db->deleteUrl(u);
	  ++mod;
	}
    }

  cout << mod << " anchor URLs removed" << endl;
  
  return mod;
}
