#include <iostream>
#include <iomanip>
#include <list>

#include "common/Config.hpp"
#include "common/Database.hpp"

#include "CleanerCommand.hpp"
#include "ListDuplicate.hpp"
#include "RemoveTslash.hpp"
#include "RemoveEmptyUrls.hpp"
#include "RenameHref.hpp"
#include "RenameSrc.hpp"
#include "RenameExtract.hpp"
#include "RemoveBlobs.hpp"
#include "RemoveAnchor.hpp"

using namespace std;

int
showModified(int m, const string& name)
{
  cout << "- '" << name << "' modified document(s) : " << m << endl;
  return m;
}

int
main(int argc, char** argv)
{
  list<CleanerCommand*> cclist;

  cclist.emplace_back(new ccRemoveTslash());
  cclist.emplace_back(new ccRenameHref());
  cclist.emplace_back(new ccRenameSrc());
  cclist.emplace_back(new ccRenameExtract());
  cclist.emplace_back(new ccRemoveEmptyUrls());
  cclist.emplace_back(new ccRemoveBlobs());
  cclist.emplace_back(new ccRemoveAnchor());

  // After all command to know if we created some
  cclist.emplace_back(new ccListDuplicate());
  
  if (argc < 2)
    {
      string prgname = argv[0];
      cout << "`" << prgname << "'" << " help :" << endl;
      cout << endl;
      for (auto& e : cclist)
	cout << "  " <<  e->getName() << ": "
	     << e->getHelp() << "." << endl;

      cout << endl
	   << "  all  : "
	   << "run all possible actions." << endl;

      
      return 0;
    }
  else
    {
      string action = argv[1];
      Config c;
      Database db(&c);
      if (action == "all")
	{
	  for (auto& e : cclist)
	    showModified(e->run(&db), e->getName());
	  return 0;
	}

      for (auto& e : cclist)
	if (e->getName() == action)
	  return showModified(e->run(&db), e->getName());
      
      cout << "Don't know how to handle '" + action + "' command." << endl;
      return -1;
    }
}
