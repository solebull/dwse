#include "Config.hpp"

// Validation
#include <boost/config.hpp>
#include <boost/program_options/detail/config_file.hpp>
#include <boost/program_options/parsers.hpp>

// Parsing
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/foreach.hpp>

#include <boost/algorithm/string/trim.hpp>

#include <iostream>
#include <fstream>
#include <exception>
#include <algorithm>
#include <regex>

using namespace std;
namespace pt = boost::property_tree;

/** Read the given config file
  *
  *
  *
  */
Config::Config(string file, bool test):
  filename(file)
{
  if (!test)
    {
      validate();
      parse();
    }
}

Config::~Config()
{

}

// Only check for config file validity
void
Config::validate()
{
  std::ifstream s(filename);
  if(!s)
    throw runtime_error("Cannot open '" + filename + "' : " + strerror(errno));
  
  std::set<std::string> options;
  options.insert("config.mongo_uri");
  options.insert("config.dbname");
  options.insert("config.data_dir");
  options.insert("config.start_pages");

  options.insert("config.start_pages");

  options.insert("nsfw.threshold");
  options.insert("nsfw.words");
  options.insert("nsfl.threshold");
  options.insert("nsfl.words");

  
  int found = 0;
  for (boost::program_options::detail::config_file_iterator i(s, options),
	 e ; i != e; ++i)
    ++found;
}

// Parse and check internal variables
void
Config::parse()
{
  pt::ptree tree;
  pt::read_ini(filename, tree);

  
  mongo_uri =  tree.get<string>("config.mongo_uri");
  dbname    = tree.get<string>("config.dbname");
  data_dir  = tree.get<string>("config.data_dir");

  fcNsfw.threshold = tree.get<int>("nsfw.threshold");
  fcNsfw.words=getWords(tree.get<string>("nsfw.words"));
  fcNsfl.threshold = tree.get<int>("nsfl.threshold");
  fcNsfl.words=getWords(tree.get<string>("nsfl.words"));
  
  // Handle start pages list
  string s = tree.get<string>("config.start_pages");
  string delimiter = ",";
  size_t pos = 0;
  string token;
  if (s.find(delimiter) == string::npos) // Not a list
    {
      start_pages.push_back(s);
    }
  else  // Comma separated list, really
    {  
      while ((pos = s.find(delimiter)) != string::npos) {
	token = s.substr(0, pos);
	boost::trim(token);
	// std::cout << "'"<< token << "'" << std::endl;
	start_pages.push_back(token);
	s.erase(0, pos + delimiter.length());
      }
    }
}

const std::string&
Config::getMongoUri(void) const
{
  return mongo_uri;
}

const std::string&
Config::getDataDir(void) const
{
  return data_dir;
}

const std::list<std::string>&
Config::getStartPages(void)const
{
  return start_pages;
}

const std::string&
Config::getDbName(void)const
{
  return dbname;
}

/** Get a list of words from the string extracted from the property-tree
  * based config file
  *
  * Needed because property tree doesn't handle lists.
  *
  */
std::list<std::string>
Config::getWords(const std::string& s)
{
  list<string> ls;
  const std::regex word("([a-zA-Z]+)");
  // Partly from https://en.cppreference.com/w/cpp/regex/regex_search
  smatch sm;
  string log = s;
  while(regex_search(log, sm, word))
    {
	ls.push_back(sm.str());
        log = sm.suffix();
    }
  return ls;
}

const FlagConfig&
Config::getNsfwFlagConfig(void)
{
  return fcNsfw;
}

const FlagConfig&
Config::getNsflFlagConfig(void)
{
  return fcNsfl;
}
  
