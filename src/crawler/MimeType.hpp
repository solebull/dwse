#ifndef __MIME_TYPE_HPP__
#define __MIME_TYPE_HPP__

#include <string>

/** Determine the mime type of a file using debian system's `file` command
  *
  *
  */
class MimeType
{
public:
  MimeType(const std::string&);

  const std::string& getFilename(void) const;
  const std::string& getMimeType(void) const;

  bool isHtml(void);
  
protected:
  std::string exec(const std::string& );
  
private:
  std::string filename;
  std::string mimetype;
};

#endif // !__MIME_TYPE_HPP__
