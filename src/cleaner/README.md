# Naming conventions

Many subclasses here. Please prefix their name with `cc` as in 
*CleanerCommand*, but don't prefix filename :

	AwesomeCommand.hpp/.cpp contains ccAwesomeCommand class!

The convention needs the Action to be first

	ls > List
	rm > Remove
	mv > Move or rename
