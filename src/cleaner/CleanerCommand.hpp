#ifndef __CLEANER_COMMAND_HPP__
#define __CLEANER_COMMAND_HPP__

#include <string>

// Forward declaration
class Database;
// End of forward declaration

/** Each command must inherits that
  *
  * Each command is responsible (i.e. must check) for creating duplicates.
  *
  *
  */
class CleanerCommand
{
public:
  CleanerCommand(const std::string&, const std::string& );
  virtual const std::string& getName(void)const;
  virtual const std::string& getHelp(void)const;

  /** Must return number of modified records */
  virtual int run(Database*)=0;
  
protected:
  /** The text that will call this command (must be non-empty and unique)
    *
    */
  std::string commandName; 
  std::string commandHelp; //!< The command help text
};

#endif // ! __CLEANER_COMMAND_HPP__
