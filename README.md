# dwse

A dark net crawler and database viewer.

[[_TOC_]]


# Dependencies

To build these project you will need these dependencies (at least on 
arch/manjaro) :

	sudo pacman -S cmake make cmake libbson-dev libmongoc-dev tor wget \
		httping libboost-program-options-dev libgtest-dev libmicrohttpd-dev \
		cloc file torsocks httping

You also must be sure you have `emsdk` binary in your PATH. If you don't,
consider install the install *emscipten SDK* :
	
	https://emscripten.org/docs/getting_started/downloads.html

On arch-based distribution such as manjaro, use :

	sudo pacman -S mongo-c-driver libmicrohttpd

# Building

	mkdir build
	cd build/
	cmake ..
	make
	make check

The last line, not mandatory, runs unit tests.

	make func

After unit tests, you can also run functionnal tests that need database 
connection.


# Config file

You must create a config file in */etc/dwse.ini* :

```
[config]
dbname      = dwse
mongo_uri   = mongodb://localhost:27017
data_dir    = /opt/dwse-data
start_pages = <ex>.onion, another, etc...

[nsfw]
threshold = 20
words     = ["worda", "wordb"]

[nsfl]
threshold = 20
words     = ["worda", "wordb"]
```

start_pages is a comma-separated list of onion addresses. You can find start
pages adresss at the hidden wiki https://thehiddenwiki.org/ page.

# Anatomy

The project consists of 3 parts :
- crawler  : crawls the dark web is search of (not ?) interisting links;
- generator: generate C++/webassembly for the website;
- www      : the result website.


Here are the connection protocols/formats :
```
+-----------------------+
|      crawler          |
+------+----------------+
       |
       | mongodb database
       |
+------v-----------------+
|      generator         |
+------+-----------------+
       |
       | C++/webassembly
       |
+------v-----------------+
|         www            |
+------------------------+
```

# Installation

Yuo can't run website from the src/www directory locally. 
[CORS](https://fr.wikipedia.org/wiki/Cross-origin_resource_sharing)
forbid using *JS* `fetch` method with file:// protocol :

From root dir :

	cd src/
	ln -s $PWD/www/ /<top-tor-content>/<new-dir-name>

## Node client

[Installation](doc/NodeClientInstallation.md)

### Dev port

	cd src/nodeclient/
	PORT=3008 npm start

## Database
### Running server

If the *mongodb* server isn't running, please try :

	sudo systemctl start mongodb
	
To view the content of the DB, you can try compass from **AUR** :

	https://aur.archlinux.org/packages/mongodb-compass

### Backup

To backup the *mongodb* database :

	mongodump -d dwse
	
It will create a `dump/dwse/` directory containing bson representation of the
full database you can backup.

## Troubleshooting

TypeError: Response has unsupported MIME type when running webpage.

If you're using apache : add application/wasm wasm to /etc/mime.types.
If you're using nginx :  in /etc/nginx/mime.types.

	    application/wasm 			  wasm;
