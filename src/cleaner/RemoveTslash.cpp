#include "RemoveTslash.hpp"

#include "common/Database.hpp"

#include <iostream>
#include <algorithm> // USES std::find()

using namespace std;

ccRemoveTslash::ccRemoveTslash()
  : CleanerCommand("rm-tslash", "Remove trailing slahes from url")
{
  
}
  
int
ccRemoveTslash::run(Database* db)
{
  // Modified records
  int mod = 0;
  std::vector<std::string> urls = db->getAllUrls();

  for (auto& e : urls)
    {
      if (e.back() == '/' || e.back() == '\\')
	{
	  string withouslash = e.substr(0, e.size()-1);
	  if (std::find(urls.begin(), urls.end(), withouslash) != urls.end())
	    {
	      db->deleteUrl(withouslash);
	      ++mod;
	    }
	}
    }

  return mod;
}
