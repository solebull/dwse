#include "Config.hpp"

#include <gtest/gtest.h>

#include <bson/bson.h>

#include "crawler/Logger.hpp"

using namespace std;

class TestableConfig: public Config
{
public:
  TestableConfig(): Config("/etc/dwse.ini", true){ }
  
  list<string> _getWords(const string& s) { return getWords(s); }
};

TEST( Config, get_words )
{
  TestableConfig tc;
  auto l = tc._getWords("[\"aze\",\"zer\"]");
  ASSERT_EQ(l.size(), 2);
  ASSERT_EQ(l.front(), "aze");
  ASSERT_EQ(l.back(), "zer");
}

TEST( Config, get_words_spaces )
{
  TestableConfig tc;
  auto l = tc._getWords("[  \"aze\",    \"zer\", \"ert\"   ]");
  ASSERT_EQ(l.size(), 3);
  ASSERT_EQ(l.front(), "aze");
  ASSERT_EQ(l.back(), "ert");
}
