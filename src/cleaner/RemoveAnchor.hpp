#ifndef __REMOVE_ANCHOR_HPP__
#define __REMOVE_ANCHOR_HPP__

#include "CleanerCommand.hpp"

// Forward declaration
class Database;
// End of forward declaration


/** Remove url containg anchor
  * 
  */
class ccRemoveAnchor : public CleanerCommand
{
public:
  ccRemoveAnchor();
  virtual int run(Database* db);
};


#endif // !__REMOVE_ANCHOR_HPP__
