#ifndef __LOGGER_HPP__
#define __LOGGER_HPP__

#include <iostream>

#define L(args) std::cerr << __FILENAME__ << ":" << __LINE__ << " - " \
  << args << std::endl;

#define LT(args) std::cerr << currentDT() <<  " - " << args;

#define REDIRECT_FILE " 2>>  crawler-errfile 1>> crawler-logfile"

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const std::string currentDT();

#endif // !< __LOGGER_HPP__

