/** Used to know if the query is unusable or must be used to query mongo
  *
  */
function isQueryEmpty(query) {
    if (query === undefined)
	return true;

    if (query.trim() === '')
	return true;

    return false;
}

module.exports = isQueryEmpty;
