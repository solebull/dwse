#ifndef __CONFIG_HPP__
#define __CONFIG_HPP__

#include <string>
#include <list>

struct FlagConfig
{
  /** The number of tested first words in the page */
  int                    threshold;
  std::list<std::string> words; //!< Word list
};

class Config
{
public:
  Config(std::string file="/etc/dwse.ini", bool test = false);
  ~Config();

  const std::string&            getMongoUri(void) const;
  const std::string&            getDbName(void) const;
  const std::string&            getDataDir(void) const;
  const std::list<std::string>& getStartPages(void)const;

  const FlagConfig& getNsfwFlagConfig(void);
  const FlagConfig& getNsflFlagConfig(void);
  
protected:
  void validate();
  void parse();

  std::list<std::string> getWords(const std::string& s);
  
  
private:
  std::string filename;   //!< The .ini config file name

  std::string mongo_uri;  //!< The mongodd server connection URI
  std::string dbname;     //!< The mongodd database name
  
  std::string            data_dir;   //!< The data directory
  std::list<std::string> start_pages; //!< Start pages

  FlagConfig fcNsfw; //!< NSFW flags
  FlagConfig fcNsfl; //!< NSFL flags
};

#endif // !__CONFIG_HPP__
