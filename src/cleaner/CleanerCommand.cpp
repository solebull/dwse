#include "CleanerCommand.hpp"

using namespace std;

CleanerCommand::CleanerCommand(const std::string& n, const std::string& h):
  commandName(n),
  commandHelp(h)
{

}

const string&
CleanerCommand::getName(void)const
{
  return commandName;
}

const string&
CleanerCommand::getHelp(void)const
{
  return commandHelp;
}
