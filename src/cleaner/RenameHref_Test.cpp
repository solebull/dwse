#include "RenameHref.hpp"

#include <gtest/gtest.h>

using namespace std;

class ccRenameHrefTest : public ccRenameHref
{
public:
  string _remove(const string& yu){  return remove(yu); }
};

TEST( ccRenameHref, remove )
{
  string o = "azeaze";
  ccRenameHrefTest r;
  
  ASSERT_EQ(r._remove("aze"), "aze");
  ASSERT_EQ(r._remove("href='aze'"), "aze");
}
