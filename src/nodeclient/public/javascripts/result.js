var words = getWords();
function updateResult(query) {
    if (query.length < 3)
	return;
    
    let suggestList = document.getElementById("suggest-list");
    suggestList.innerHTML = "";
    var result = new Array();
    words.map(function(algo){
        query.split(" ").map(function (word){
            if(algo.toLowerCase().indexOf(word.toLowerCase()) != -1){
                result.push(algo);
            }               
	});
        let resultnb = document.getElementById("nbmatches");
        resultnb.innerHTML = result.length.toString();
    });
    
    // Only prints 100 first results
    result.slice(0, 20).forEach(function(e){
        suggestList.innerHTML +=
            `<li class="list-group-item">${e}</li>`;
    });
}


/** Execute the query
  *
  * We don't have lmongo connection here, only server does.
  * We need to reload 
  *
  * \param lucky If set to truee, will open the first result automatically.
  *
  */
function onQuery(lucky) {
    let query = document.getElementById("idquery").value;
    if (lucky)
	window.location.href = "/?lucky=1&q=" + query;
    else
	window.location.href = "/?q=" + query;
}

