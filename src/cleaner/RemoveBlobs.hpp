#ifndef __REMOVE_CLOBS_HPP__
#define __REMOVE_CLOBS_HPP__

#include "CleanerCommand.hpp"

#include <list>
#include <string>

// Forward declaration
class Database;
// End of forward declaration

/** Remove empty urls from the database
  * 
  */
class ccRemoveBlobs : public CleanerCommand
{
public:
  ccRemoveBlobs();
  virtual int run(Database* db);

protected:
  bool isExtension(const std::string&, const std::string&) const;
  
private:
  /// List to-be-removed extensions (end of strings)
  std::list<std::string> extlist; 
};


#endif // !__REMOVE_CLOBS_HPP__
