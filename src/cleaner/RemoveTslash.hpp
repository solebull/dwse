#ifndef __REMOVE_TSLASH_HPP__
#define __REMOVE_TSLASH_HPP__

#include "CleanerCommand.hpp"

// Forward declaration
class Database;
// End of forward declaration


/** Remove trailing slash from a given URL$
  * 
  */
class ccRemoveTslash : public CleanerCommand
{
public:
  ccRemoveTslash();
  virtual int run(Database* db);
};


#endif // !__REMOVE_TSLASH_HPP__
