#ifndef __DOCUMENT_HPP__
#define __DOCUMENT_HPP__

#include <list>
#include <set>
#include <string>
#include <fstream>

#include "common/Url.hpp"


// Forward declarations
class Database;
// End of forward declarations

/** The full to-be-compiled content
  *
  */
class Document
{
public:
  Document(Database*, bool test=false);
  
  friend std::ostream& operator<< (std::ostream& stream, const Document& u) {
    u.print(stream);

    return stream;
  }

  void to_js(std::ostream&);
  
protected:
  void print(std::ostream&)const;
  
private:
  //!< Keep a track of already added words from url to avoid duplicates
  std::list<std::string>  addedUrls; 
  std::list<std::string>  startPages;
  std::set<std::string>   words;   //!< A unique+sorted list of words
};

#endif // !__DOCUMENT_HPP__
