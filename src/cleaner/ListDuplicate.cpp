#include "ListDuplicate.hpp"

#include "common/Database.hpp"

#include <iostream>

using namespace std;

ccListDuplicate::ccListDuplicate():
  CleanerCommand("ls-dup",
		 "List duplicates documents from database from known schemes")
{

}

int
ccListDuplicate::run(Database* db)
{
  auto l = db->getAllUrls();
  
  int found = 0;
  for (auto& u : l)
    if (count(l, u) > 1)
      ++found;
  
  if (found > 0)
    cout << "Warning : found duplicates : " << found << endl;
  else
    cout << "No duplicate found." << endl;
  
  return 0;
}

int
ccListDuplicate::count(vector<string> li, string u)
{
  int count = 0;
  for (auto it = li.begin(); it != li.end(); ++it)
    if (*it == u) ++count;
  return count;
}
