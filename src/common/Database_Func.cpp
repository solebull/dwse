#include "Database.hpp"
#include "Domain.hpp"
#include "Config.hpp"
#include "Url.hpp"
#include <gtest/gtest.h>

#include <string>
#include <list>

#include <cuchar>

using namespace std;

class TestableDatabase : public Database
{
public:
  TestableDatabase():
    Database(new Config(), true)
  {

  }
  string _toAscii(const string& s)const { return toAscii(s); }
  string _toBsonStr(const string& s)const { return toBsonStr(s); }
  
  ~TestableDatabase(){}
};

TEST( Database, upsert_url )
{
  string url = "test-url";
  TestableDatabase db;
  db.upsertUrl(url);
  // If this fails, please be sure mongod service is running
  ASSERT_TRUE( db.getUrl(url) != NULL ); 
}
  
TEST( Database, delete_url )
{
  string url = "test-url";
  TestableDatabase db;
  db.deleteUrl(url);
  auto url2 = db.getUrl(url);
  ASSERT_TRUE(url2 == NULL);
}

TEST( Database, crawled_date )
{
  // A newly cerated Url returns 0
  Url zurl;
  ASSERT_EQ(zurl.getCrawledTime(), 0);
  
  // Not 0
  string url = "test-url";
  TestableDatabase db;
  db.upsertUrl(url);
  time_t T = db.setCrawlTime(url);
  auto url2 = db.getUrl(url);
  ASSERT_EQ(url2->getCrawledTime(), T);
  db.deleteUrl(url);
}

TEST( Database, set_title )
{
  Url zurl;
  ASSERT_TRUE(zurl.getTitle().empty());
  
  // Not 0
  string url = "test-url";
  string tt = "test-title";
  TestableDatabase db;
  db.upsertUrl(url);
  db.setTitle(url, tt);
  auto url2 = db.getUrl(url);
  ASSERT_TRUE(url2->getTitle() == tt);
  db.deleteUrl(url);
}

TEST( Database, inc_death )
{
  Url zurl;
  ASSERT_EQ(zurl.getDeathNb(), 0);
  
  string url = "test-url";
  TestableDatabase db;
  db.upsertUrl(url);
  db.incDeath(url);
  auto url2 = db.getUrl(url);
  ASSERT_EQ(url2->getDeathNb(), 1);

  db.incDeath(url);
  auto url3 = db.getUrl(url);
  ASSERT_EQ(url3->getDeathNb(), 2);

  db.deleteUrl(url);
}

TEST( Database, back_online )
{
  string url = "test-url";
  Url zurl;
  ASSERT_EQ(zurl.getLiveBackAfter(), 0);
  
  TestableDatabase db;
  db.upsertUrl(url);
  db.incDeath(url);
  db.incDeath(url);
  db.setBackOnline(url);
  auto url2 = db.getUrl(url);

  ASSERT_EQ(url2->getDeathNb(), 0);
  ASSERT_EQ(url2->getLiveBackAfter(), 2);

  db.deleteUrl(url);
}

// Correctly extract document's mongo _id
TEST( Database, extract_id )
{
  string url = "test-url";
  Url zurl;
  auto i = zurl.getId();
  TestableDatabase db;
  db.upsertUrl(url);

  auto url2 = db.getUrl(url);

  /*  char str[25];
  bson_oid_to_string (url2->getId(), str);
  cout << "OID: " << str << endl;
  */
  
  ASSERT_TRUE(url2->getId() != i);
  db.deleteUrl(url);
}

// Modify the url identified by a given oid and keep all its fields
TEST( Database, modify_url )
{
  string url = "test-url/";
  string newUrl = "test-url-new";
  Url zurl;
  auto i = zurl.getId();
  TestableDatabase db;
  db.upsertUrl(url);

  auto url2 = db.getUrl(url);
  ASSERT_TRUE(url2->getOnion() == url);


  db.modifyUrl(url2->getId(), newUrl);

  // Can't find anymore, disappeared
  auto url3 = db.getUrl(url);  
  ASSERT_TRUE(url3 == NULL);

  auto url4 = db.getUrl(newUrl);  
  ASSERT_TRUE(url2->getId() != url4->getId());
  ASSERT_TRUE(url4->getOnion() == newUrl);
  
  ASSERT_TRUE(url2->getId() != i);
  db.deleteUrl(newUrl);
}

// Modify the url and see if it keeps the title and other fields
TEST( Database, modify_url_title )
{
  string url = "modify_url_title/";
  string newUrl = "modify_url_title_test-url-new";
  string title = "Title";

  Url zurl;
  auto i = zurl.getId();
  TestableDatabase db;
  db.upsertUrl(url);
  db.setTitle(url, title);
  
  auto url2 = db.getUrl(url);
  db.modifyUrl(url2->getId(), newUrl);

  auto url4 = db.getUrl(newUrl);
  ASSERT_TRUE(url4->getTitle() == title);
  db.deleteUrl(newUrl);
}

// Shouldn't accept empty urls
TEST( Database, empty_url )
{
  TestableDatabase db;
  int count1 = db.getAllUrls().size();
  db.upsertUrl("");
  int count2 = db.getAllUrls().size();
  ASSERT_EQ(count1, count2);
}

// To delete empty url, we must be able to delete by ID
TEST( Database, delete_by_id )
{
  string url = "modify_url_title/";
  TestableDatabase db;
  int count1 = db.getAllUrls().size();

  db.upsertUrl(url);
  auto urlo = db.getUrl(url);
  int count2 = db.getAllUrls().size();
  ASSERT_EQ(count1 + 1, count2);
  db.deleteById(urlo->getId());
  
  int count3 = db.getAllUrls().size();
  ASSERT_EQ(count1, count3);
}

// Return an empty word list if not set
TEST( Database, add_words_none )
{
  string url = "modify_url_title/";
  TestableDatabase db;
  db.upsertUrl(url);

  auto url4 = db.getUrl(url);
  auto wl = db.getWords(url4->getId());
    
  ASSERT_EQ(wl.size(), 0);   // Only two words by group
  db.deleteUrl(url);
}


// Modify the url and see if it keeps the title and other fields
TEST( Database, add_words )
{
  string url = "modify_url_title/";
  TestableDatabase db;
  db.upsertUrl(url);

  list<string> w;
  w.push_back("aze");
  w.push_back("aze");
  w.push_back("aze");
  w.push_back("zer");
  w.push_back("zer");

  db.addWords(url, w);

  auto url4 = db.getUrl(url);
  auto wl = db.getWords(url4->getId());
  ASSERT_EQ(wl.size(), 2);   // Only two words by group
  auto wa = wl.front();
  ASSERT_EQ(wa.w, "aze");
  ASSERT_EQ(wa.n, 3);
  db.deleteUrl(url);
}

// How the database handle adding words to the same url twice
TEST( Database, add_words_twice )
{
  string url = "modify_url_title/";
  TestableDatabase db;
  db.upsertUrl(url);

  list<string> w;
  w.push_back("aze");
  w.push_back("aze");
  w.push_back("aze");
  w.push_back("zer");
  w.push_back("zer");

  db.addWords(url, w);
  // twice
  db.addWords(url, w);

  auto url4 = db.getUrl(url);
  auto wl = db.getWords(url4->getId());
  ASSERT_EQ(wl.size(), 2);   // Only two words by group
  auto wa = wl.front();
  ASSERT_EQ(wa.w, "aze");
  ASSERT_EQ(wa.n, 3);
  db.deleteUrl(url);
}

// We finally need a function that remove non-ascii chars
TEST(Database, add_word_to_ascii)
{
  string c = "\xe4nderungen";
  TestableDatabase db;
  ASSERT_EQ(db._toAscii(c), "nderungen");
  
}

TEST(Database, add_word_non_ascii)
{

  string url = "modify_url_title/";
  TestableDatabase db;
  db.upsertUrl(url);

  string c = "\xe4nderungen";
  std::mbstate_t state{};
  char16_t a[20];
  // https://en.cppreference.com/w/cpp/string/multibyte/mbrtoc8
  std::size_t s =  mbrtoc16( a,   // char8_t* pc8
		       c.c_str(),
		       c.size() - 1,
		       &state );
  list<string> w;
  w.push_back("\xe4nderungen");
  db.addWords(url, w);

  auto url4 = db.getUrl(url);
  auto wl = db.getWords(url4->getId());
  ASSERT_EQ(wl.size(), 1);   // Only two words by group
  
  db.deleteUrl(url);
}

// Try to add an empty word
TEST(Database, add_word_empty)
{
  string url = "modify_url_title/";
  TestableDatabase db;
  db.upsertUrl(url);

  list<string> w;
  w.push_back("");
  w.push_back("\xe4\xe6");  // Will be empty
  db.addWords(url, w);

  auto url4 = db.getUrl(url);
  auto wl = db.getWords(url4->getId());
  ASSERT_EQ(wl.size(), 0);  // Not added (empty)
  db.deleteUrl(url);
}

// Try to add an empty word
TEST(Database, add_blob)
{
  string url = "modify_url_title/";
  TestableDatabase db;
  int nb = db.getBlobsNumber();
  // We can't have real mime-type here, 
  db.upsertBlob(url, "type");

  ASSERT_EQ(db.getBlobsNumber(), ++nb);
  db.deleteBlob(url);
}

// upsertBlob shoudn't add duplicate
TEST(Database, add_blob_duplicate)
{
  string url = "modify_url_title/";
  TestableDatabase db;
  // We can't have real mime-type here, 
  db.upsertBlob(url, "type");
  int nb = db.getBlobsNumber();
  db.upsertBlob(url, "type2");

  ASSERT_EQ(db.getBlobsNumber(), nb);
  db.deleteBlob(url);
}

TEST(Database, get_all_domains)
{
  TestableDatabase db;
  auto ad = db.getDomainsNumber();

  ASSERT_EQ(ad, 0);
}
  
TEST(Database, upsert_domain)
{
  string url = "aze.onion";
  TestableDatabase db;
  auto ad1 = db.getDomainsNumber();
  db.upsertDomain(url);
  auto ad2 = db.getDomainsNumber();

  ASSERT_EQ(ad1 + 1, ad2);
  db.deleteDomain(url);
}

// Try to reproduce a crawler issue with [ ] brackets
TEST(Database, to_bson_str)
{
  TestableDatabase db;
  ASSERT_EQ( db._toBsonStr("["), "\\["); 
  ASSERT_EQ( db._toBsonStr("]"), "\\]"); 
  ASSERT_EQ( db._toBsonStr("aze]"), "aze\\]"); 
  ASSERT_EQ( db._toBsonStr("]aze"), "\\]aze"); 
}

// Try to reproduce a crawler issue with [ ] brackets
TEST(Database, upsert_url_square_brackets)
{
  string url = "[BASH SCRIPT] Auto_wps_crack - Foro Hackplayers";
  TestableDatabase db;
  db.upsertUrl(url);
  // If this fails, please be sure mongod service is running
  ASSERT_TRUE( db.getUrl(url) != NULL ); 
  db.deleteUrl(url);
}

// Try to reproduce a crawler issue with [ ] brackets
TEST(Database, set_flags_to_url)
{
  string url = "test";
  TestableDatabase db;
  db.upsertUrl(url);
  set<string> flags = { "aze", "zer" };
  
  Url* u = db.getUrl(url);
  db.setFlags(u, flags);

  auto fl = db.getFlags(u);
  // If this fails, please be sure mongod service is running
  ASSERT_EQ( fl.size(), 2 ); 
  ASSERT_EQ( *fl.begin(), "aze" ); 

  db.deleteUrl(url);
}

// Try a getFlags call on an url without flags array. Shouldn't crash
TEST(Database, get_null_flags)
{
  string url = "test";
  TestableDatabase db;
  db.upsertUrl(url);
  
  Url* u = db.getUrl(url);

  auto fl = db.getFlags(u);
  // If this fails, please be sure mongod service is running
  ASSERT_EQ( fl.size(), 0 ); 

  db.deleteUrl(url);
}

// Try to reproduce a crawler issue with [ ] brackets
TEST(Database, set_flags_to_domain)
{
  string dom = "test";
  TestableDatabase db;
  db.upsertDomain(dom);
  set<string> flags = { "aze", "zer" };
  
  Domain d(dom);
  db.setFlags(&d, flags);

  auto fl = db.getFlags(&d);
  // If this fails, please be sure mongod service is running
  ASSERT_EQ( fl.size(), 2 ); 
  ASSERT_EQ( *fl.begin(), "aze" ); 

  db.deleteDomain(dom);
}

// Shouldn't add empty domain names
TEST(Database, upsert_empty_domain)
{
  string url = "";
  TestableDatabase db;
  auto ad1 = db.getDomainsNumber();
  db.upsertDomain(url);
  auto ad2 = db.getDomainsNumber();
  ASSERT_EQ(ad1, ad2);
}

// Shouldn't add empty/spaced domain names
TEST(Database, upsert_space_domain)
{
  string url = "    ";
  TestableDatabase db;
  auto ad1 = db.getDomainsNumber();
  db.upsertDomain(url);
  auto ad2 = db.getDomainsNumber();
  ASSERT_EQ(ad1, ad2);
}

// Shouldn't have a "dom" field
TEST(Database, upsert_space_has_dom)
{
  string url = "no-dom-field";
  TestableDatabase db;
  db.upsertDomain(url);

  Domain* d = db.getDomain(url);
  ASSERT_TRUE(d != NULL);

  string s = d->toStr();
  ASSERT_NE(s.find("domain"), string::npos);
  ASSERT_EQ(s.find("dom\""), string::npos);
  
  auto ad1 = db.getDomainsNumber();
  db.deleteDomain(url);
}

// Adding flags should set a flag-type field (to auto first)
TEST(Database, flags_url_set_type)
{
  string url = "test";
  TestableDatabase db;
  db.upsertUrl(url);
  set<string> flags = { "aze", "zer" };
  
  Url* u = db.getUrl(url);
  db.setFlags(u, flags);

  /*  auto fl = db.getFlags(u);
   */
  Url* u2 = db.getUrl(url);
  string s = u2->toStr();
  cout << s << endl;
  ASSERT_NE(s.find("flag-type"), string::npos);
  ASSERT_NE(s.find("auto"), string::npos);

  db.deleteUrl(url);
}

// Adding flags should set a flag-type field (to auto first)
TEST(Database, flags_domain_set_type)
{

  string dom = "test";
  TestableDatabase db;
  db.upsertDomain(dom);
  set<string> flags = { "aze", "zer" };
  
  Domain d(dom);
  db.setFlags(&d, flags);

  Domain* d2 = db.getDomain(dom);
  string s = d2->toStr();
  cout << s << endl;
  ASSERT_NE(s.find("flag-type"), string::npos);
  ASSERT_NE(s.find("auto"), string::npos);

  db.deleteDomain(dom);
}

