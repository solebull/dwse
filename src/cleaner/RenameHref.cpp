#include "RenameHref.hpp"

#include "common/Database.hpp"
#include "common/Url.hpp"

#include <algorithm> // USES std::find()
#include <iostream>

using namespace std;

ccRenameHref::ccRenameHref():
  CleanerCommand("rm-href", "Rename invalid href-nested urls")
{

}

int
ccRenameHref::run(Database* db)
{
  int mod = 0; // Number of modified records
  auto urls = db->getAllUrls();

  for (auto& u : urls)
    {
      if (u.find("href='") != std::string::npos &&
	  u.back() == '\'')
	{
	  auto zurl = db->getUrl(u);
	  db->modifyUrl(zurl->getId(), remove(u));
	  
	  mod++;
	}
    }
    return mod;
}

std::string
ccRenameHref::remove(const std::string& s)
{
  if (s.find("href='") != std::string::npos && s.back() == '\'')
    {
      return s.substr(6, s.size() - 7);
    }

  return s;
}
