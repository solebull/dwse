#include "MimeType.hpp"

#include <gtest/gtest.h>
#include <stdexcept>

#define FILE "dwse-crawler"

/// A filenamed ctor exists
TEST( MimeType,  ctor )
{
  auto a = new MimeType(FILE);
  ASSERT_TRUE(a != NULL);
}

/// Has a getFilename command
TEST( MimeType,  getFilename )
{
  auto a = new MimeType(FILE);
  ASSERT_EQ(a->getFilename(), FILE);
}

TEST( MimeType,  getMimetype )
{
  auto a = new MimeType(FILE);
  ASSERT_FALSE(a->getMimeType().empty());
}

TEST( MimeType,  fixtureHTMLFile )
{
  auto a = new MimeType("../fixture/mime/test.html");
  ASSERT_EQ(a->getMimeType(), "text/html");
}

TEST( MimeType,  fixtureJPGFile )
{
  auto a = new MimeType("../fixture/mime/test.jpg");
  ASSERT_EQ(a->getMimeType(), "image/jpeg");
}

TEST( MimeType,  fixturePNGFile )
{
  auto a = new MimeType("../fixture/mime/test.png");
  ASSERT_EQ(a->getMimeType(), "image/png");
}

TEST( MimeType,  throw_on_non_existing_file )
{
  ASSERT_THROW (new MimeType("../fixture/mime/NON_existing_FILE.png"),
		std::runtime_error);
  
}

TEST( MimeType,  is_html )
{
  auto a = new MimeType("../fixture/mime/test.html");
  ASSERT_TRUE(a->isHtml());

  auto b = new MimeType("../fixture/mime/test.jpg");
  ASSERT_FALSE(b->isHtml());
}


